<link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
     <div class="container-fluid">

     	 <div class="row">

               <div class="col-xl-6 mx-auto text-center">
                  <div class="section-title mb-100">
                     
                     <h4>How We Work</h4>
                  </div>
               </div>
            </div>
     	 <div class="container-fluid">
     	 	 <div class="container-fluid">
     	 	 	 <div class="container-fluid">

	
	<div class="container"><div class="row">
		
<div class="col-md-4" align="center">
			<p>
				<strong>SOLUTIONS</strong><br/>
Our team of engineers are here to help our clients create software solutions. If you belong to an industry that is a part of our concern, we will help you find the most optimal- manageable and updated solutions using latest tech stacks.
</small>
			</p>
		</div>
		<div class="col-md-4" align="center"><h5 class="header--subtitle" align="center">Have an Idea ? <br>
Have a plan ? </h5><a class="btnv7 btnv7-primaryOnDarkBackground js-scrollToLink" href="#sign-up-form">Work With Us</a><br/><br>
			<img alt="Bootstrap Image Preview" src="images/how_we_work.png" height="110" width="190" class="rounded">
		</div>
		<div class="col-md-4" align="center">
			<p>
				<strong>INNOVATION</strong><br/>
Our team of engineers are always researching to innovate by building specialized in-house products for our industries of concern. We also welcome, people with ideas to come to us & collaboratively innovate by using our resources. 

			</p>
		</div>
	</div>
	</div>
</div>
</div>
	</div>
</div>

	<script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>