<DOCTYPE html>
<html>
   <head>
       <!--Redirect to different pages based on screen size -->
       <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
$(document).ready(function(){
  if($(window).width() <= 750) {
window.location = "/mobile/teams.php";
}
  if($(window).width() > 750 && $(window).width() < 1000 ) {
window.location = "/tab/teams.php";
}
});
</script>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>MedinaTech || Culture & Career Development</title>
      <link href="https://fonts.googleapis.com/css?family=Poppins:600,700|Roboto&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="style_MT.css">
      <script src="functions_MT.js"></script>
      <!-- <link rel="stylesheet" type="text/css" href="css/videocontrols.css" /> -->
   </head>
   <body>

      <!-- --------------------tab menu------------------------- -->
      <div class="tabMenu">
         <div>
            <a href="index.php"><img class="tabMenu_logo" src="skins/MedinaTech PNG.png" alt="logo"></a>
         </div>
         <ul>
            <li><a  href="index.php"><b>Home Page</b></a></li>
            <li><a class="active" href="#"><b>Culture & Career Development</b></a></li>
            <li><a href="ReachUs_MT.php"><b>Reach Us</b></a></li>
         </ul>
      </div>
      <!-- ---------------mobile menu---------------------------------- -->
      <div class="mobileMenu">
         <a href="index.php"><img style="width: 140px; position: absolute;padding: 2px 10px;  z-index: -2" src="skins/MedinaTech PNG.png" alt="logo"></a>
         <div class="dropdown">
            <img onclick="myFunction()" class="dropbtn" style="width: 25px" src="skins/menu.png">
            <div id="myDropdown" class="dropdown-content">
               <a class="" href="index.php">Home</a>
               <a class="active" href="#">Culture & Career Development</a>
               <a href="ReachUs_MT.php">Reach Us</a>
            </div>
         </div>
      </div>
      <!-- ---------------pc navbar---------------------------------- -->
      <div class="navbar">
         <ul>
            <li><a href="ReachUs_MT.php"><b>Reach Us</b></a></li>
            <li><a class="active" href="#"><b>Culture & Career Development</b></a></li>
            <li><a class="" href="index.php"><b>Home Page</b></a></li>
         </ul>
         <a href="index.php"><img class="lionLogo" src="skins/lion.png" alt="logo"></a>
      </div>
      <!-- ------------------------------------ -->
      <!--<div class="social">-->
         <!--<a href="http://www.facebook.com/sharer/sharer.php?u=https://medinatech.co" target="_blank"><img src="skins/fb.png" alt="logo"></a>-->
         <!--<a href="https://www.linkedin.com/shareArticle?mini=true&url=https%3A%2F%2Fwww.medinatech.co&title=Medina+Tech" target="_blank"><img src="skins/in.png" alt="logo">  </a>-->
      <!--</div>-->
      <!-- --------------------------------------- -->

        <!-- --------------------------------------- -->
        <section>
            <!-- <div class="gridFull">
            <div class="allItem"> -->
            <!-- <div class="cultureHeader"> -->
            <img style="width: 100%; right: 0px; z-index: -1;cursor: not-allowed;" src="skins/Group 15.png" alt="Team Header" >
            <p class="heading" style="margin-top: -9%; font-size: 45px; margin-left:80px;">Culture & Career Development</p>
            <!-- </div> -->
            <!-- 
            </div>
            </div> -->
        </section>
        <br>
        <br>
        <br>
        <br>
        <div class="container" align="center">
            <!--  <div class="grid2"> -->
            <section>
                <font align="center" size="3px" style="font:  16px/25px Roboto;">
               A startup culture is a workplace environment that values  
               
             </font>
             <br>
                <font size="5px" style="font: Bold 20px/33px Roboto;">
               <b>
               Creative problem solving, Open communication and A flat hierarchy.
               </b>
               </font>
                <br>
                <br>
                <img style="width: 40%; right: 0px; z-index: -1;cursor: not-allowed;" src="skins/a-01@2x.png" alt="Skins">
            </section>
            <!--     </div> -->
        </div>
        <!-- </div> -->
        <br>
        <br>
            <br>
            <!--Career Section Start-->
        <div class="container2" align="center">
            <font align="center" size="3px" style="font: 16px/25px Roboto;">
               We prioritize comfort & convenience of every team member through 
               <br/>
             </font>
            <font size="5px" style="font: Bold 25px/33px Roboto;">
               <b>
               Efficient Work Environment | Empowerment & Career Development
               </b>
               </font>
            <br>
            <br>
            <div class="rowt" style="padding-top: 30px">
                <div class="columnt" align="right">
                    <img src="images/efficient.png" alt="Snow" style="width:60%;cursor: not-allowed;" align="right">
                </div>
                <div class="columnt" align="left">
                    <a href="#popup1">
                        <img src="images/empowerment.png"  alt="Empowerment" style="width:60%;cursor: pointer;" align="left">
                    </a>
                </div>
         
            </div>
        </div>
       
               <!-- Modal -->
               <style>
/*body {*/
/*  font-family: Arial, sans-serif;*/
/*  background: url(http://www.shukatsu-note.com/wp-content/uploads/2014/12/computer-564136_1280.jpg) no-repeat;*/
/*  background-size: cover;*/
/*  height: 100vh;*/
/*}*/

h1 {
  text-align: center;
  font-family: Tahoma, Arial, sans-serif;
  color: #06D85F;
  margin: 80px 0;
}

.box {
  width: 40%;
  margin: 0 auto;
  background: rgba(255,255,255,0.2);
  padding: 35px;
  border: 2px solid #fff;
  border-radius: 20px/50px;
  background-clip: padding-box;
  text-align: center;
}

.button {
  font-size: 1em;
  padding: 10px;
  color: #fff;
  border: 2px solid #06D85F;
  border-radius: 20px/50px;
  text-decoration: none;
  cursor: pointer;
  transition: all 0.3s ease-out;
}
.button:hover {
  background: #06D85F;
}

.overlay {
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background: rgba(0, 0, 0, 0.7);
  transition: opacity 500ms;
  visibility: hidden;
  opacity: 0;
}
.overlay:target {
  visibility: visible;
  opacity: 1;
}

.popup {
  margin: 70px auto;
  padding: 20px;
  background: #fff;
  border-radius: 5px;
  width: 30%;
  position: relative;
  transition: all 5s ease-in-out;
}

.popup h2 {
  margin-top: 0;
  color: #333;
  font-family: Tahoma, Arial, sans-serif;
}
.popup .close {
  position: absolute;
  top: 20px;
  right: 30px;
  transition: all 200ms;
  font-size: 30px;
  font-weight: bold;
  text-decoration: none;
  color: #333;
}
.popup .close:hover {
  color: #06D85F;
}
.popup .content {
  max-height: 30%;
  overflow: auto;
}

@media screen and (max-width: 700px){
  .box{
    width: 70%;
  }
  .popup{
    width: 70%;
  }
}
               </style>
<div id="popup1" class="overlay">
	<div class="popup">
		<h2>Alert !</h2>
		<a class="close" href="#">&times;</a>
		<div class="content">
		    <br>
		    
		    <br>
			We are currently not accepting any internships due to changes in our project scope for the pandemic.
		</div>
	</div>
</div>
        <!--TEAM CONTENT-->
        <!--add in team file-->
        <!---->

        </div>
    <!--Footer Section -->
        <?php
         include "footer_new.php";
         ?>
    </body>

    </html>