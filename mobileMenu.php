<DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>MedinaTech</title>
	<link href="https://fonts.googleapis.com/css?family=Poppins:600,700|Roboto&display=swap" rel="stylesheet"> 

	<link rel="stylesheet" href="style_MT.css">
	<script src="functions_MT.js"></script>
</head>
<body>

	<!-- --------------------tab menu------------------------- -->
	
	<div class="tabMenu">

		

		<div>
			<img class="tabMenu_logo" src="skins/MedinaTech PNG.png">
		</div>

		<ul>

			<li><a class="active" href="home_MT.php"><b>Home Page</b></a></li>
   	 		<li><a href="Culture page_MT.php"><b>Culture & Career Development</b></a></li>
   	 		<li><a href="#"><b>Reach Us</b></a></li>

    	</ul>



	</div>



	<!-- ---------------mobile menu---------------------------------- -->

	<div class="mobileMenu">

		<img style="width: 140px; position: absolute;padding: 2px 10px;" src="skins/MedinaTech PNG.png">

		<div class="dropdown">
		  <img onclick="myFunction()" class="dropbtn" style="width: 25px" src="skins/menu.png">
		  
		  <div id="myDropdown" class="dropdown-content">
		    <a class="active" href="#home">Home</a>
		    <a href="#about">Culture & Career Development</a>
		    <a href="#contact">Reach Us</a>
		  </div>
		</div>
	</div>

	

	



</body>
</html>