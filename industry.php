<!-- <div class="row">

               <div class="col-xl-6 mx-auto text-center">
                  <div class="section-title mb-100">
                     
                     <h4>Industries We Prioritize</h4>
                  </div>
               </div>
            </div>
            <div class="splashSection--row splashSection--row-fullWidth js-assignmentStepsTabsContainer"><div class="splashTabList"><div class="splashTabList--tabsContainer"><button class="splashTab js-tab splashTab-is-selected" data-tab-idx="0" style="width: 120px"><div class="splashTab--icon splashTab--icon-unselected"><svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 20 24" width="20" stroke="currentColor"><g style="fill:none;fill-rule:evenodd;stroke-linecap:round;stroke-linejoin:round;stroke-width:2"><path d="m19 9h-8v-8"></path><path d="m5 14h10"></path><path d="m5 18h10"></path><path d="m17 23h-14c-1.105 0-2-.895-2-2v-18c0-1.105.895-2 2-2h8l8 8v12c0 1.105-.896 2-2 2z"></path></g></svg>
</div><div class="splashTab--icon splashTab--icon-selected"><svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 20 24" width="20" fill="currentColor"><path d="m14 0v6h6zm-2 0h-10c-1.104 0-2 .896-2 2v20c0 1.104.896 2 2 2h16c1.104 0 2-.896 2-2v-14h-8zm3 20h-10c-.55 0-1-.45-1-1s.45-1 1-1h10c.55 0 1 .45 1 1s-.45 1-1 1zm0-7c.55 0 1 .45 1 1s-.45 1-1 1h-10c-.55 0-1-.45-1-1s.45-1 1-1z"></path></svg>
</div> <a class=" active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Education</a></button><button class="splashTab js-tab" data-tab-idx="1" style="width: 120px"><div class="splashTab--icon splashTab--icon-unselected"><svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 20 24" width="20" stroke="currentColor"><g style="fill:none;fill-rule:evenodd;stroke-linecap:round;stroke-linejoin:round;stroke-width:2"><path d="m9 14h6"></path><path d="m9 8h6"></path><path d="m5 1v22"></path><path d="m17.004 23h-16.004v-21.996h16.004c1.102 0 1.996.894 1.996 1.996v18.004c0 1.102-.894 1.996-1.996 1.996z"></path></g></svg>
</div><div class="splashTab--icon splashTab--icon-selected"><svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 20 24" width="20" fill="currentColor"><path d="m0 2v20c0 1.104.895 2 2 2h1v-24h-1c-1.105 0-2 .896-2 2zm18-2h-13v24h13c1.105 0 2-.896 2-2v-20c0-1.104-.895-2-2-2zm-1.75 14h-7.5c-.412 0-.75-.45-.75-1s.338-1 .75-1h7.5c.412 0 .75.45.75 1s-.337 1-.75 1zm0-5h-7.5c-.412 0-.75-.45-.75-1s.338-1 .75-1h7.5c.413 0 .75.45.75 1s-.337 1-.75 1z"></path></svg>
</div><a id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Infrastructure</a></button><button class="splashTab js-tab" data-tab-idx="2" style="width: 120px"><div class="splashTab--icon splashTab--icon-unselected"><svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24" stroke="currentColor"><g style="fill:none;fill-rule:evenodd;stroke-linecap:round;stroke-linejoin:round;stroke-width:2"><path d="m23 23h-22v-20c0-1.105.896-2 2-2h18c1.105 0 2 .896 2 2z"></path><path d="m8.5 11-3 3 3 3"></path><path d="m14.5 17 3-3-3-3"></path><path d="m5 5.032v-.064"></path><path d="m9 5.032v-.064"></path><path d="m13 5.032v-.064"></path></g></svg>
</div><div class="splashTab--icon splashTab--icon-selected"><svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24" fill="currentColor"><path d="m20.571 0h-17.14c-1.895 0-3.431 1.536-3.431 3.431v4.569h24v-4.571c0-1.894-1.535-3.429-3.429-3.429zm-16.071 6c-.828 0-1.5-.671-1.5-1.5s.672-1.5 1.5-1.5 1.5.671 1.5 1.5-.672 1.5-1.5 1.5zm5 0c-.828 0-1.5-.671-1.5-1.5s.672-1.5 1.5-1.5 1.5.671 1.5 1.5-.672 1.5-1.5 1.5zm5 0c-.828 0-1.5-.671-1.5-1.5s.672-1.5 1.5-1.5 1.5.671 1.5 1.5-.672 1.5-1.5 1.5zm-13.5 18h22c.5522847 0 1-.4477153 1-1v-13h-24v13c0 .5522847.44771525 1 1 1zm12.879-9.293c-.391-.391-.391-1.023 0-1.414s1.023-.391 1.414 0l3.707 3.707-3.707 3.707c-.195.195-.451.293-.707.293s-.512-.098-.707-.293c-.391-.391-.391-1.023 0-1.414l2.293-2.293zm-5.172-1.414c.391-.391 1.023-.391 1.414 0s.391 1.023 0 1.414l-2.293 2.293 2.293 2.293c.391.391.391 1.023 0 1.414-.195.195-.451.293-.707.293s-.512-.098-.707-.293l-3.707-3.707z"></path></svg>
</div> <a id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Finance</a></button></div></div><div class="splashTabContents js-tabContents splashTabContents-fullWidth splashTabContents-is-visible" data-tab-contents-for-idx="0"><div class="processSteps processSteps-splash">

<div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent" align="center">
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                     <div class="processSteps processSteps-splash"><div class="processStep"><div class="processStep--imgContainer"><img alt="Scan Student Work" class="processStep--img" src="images/industries/games.png"><div class="processStep--stepNumber">1</div></div><div class="processStep--textContainer"><div class="processStep--heading">Educational Games for Children</div><div class="processStep--explanation"></div></div></div><div class="processStep"><div class="processStep--imgContainer"><img alt="Grade Submissions" class="processStep--img" src="images/industries/lms.png"><div class="processStep--stepNumber">2</div></div><div class="processStep--textContainer"><div class="processStep--heading">Learning Management Systems</div><div class="processStep--explanation"></div></div></div><div class="processStep"><div class="processStep--imgContainer"><img alt="Send &amp; Export Grades" class="processStep--img" src="images/industries/dc.png"><div class="processStep--stepNumber">3</div></div><div class="processStep--textContainer"><div class="processStep--heading">Digital Classrooms</div><div class="processStep--explanation"></div></div></div><div class="processStep"><div class="processStep--imgContainer"><img alt="Get Detailed Analytics" class="processStep--img" src="images/industries/ct.png"><div class="processStep--stepNumber">4</div></div><div class="processStep--textContainer"><div class="processStep--heading">Corporate Training Materials</div><div class="processStep--explanation"></div></div></div></div>
                    </div>
                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab" align="center">
                    <div class="processSteps processSteps-splash"><div class="processStep"><div class="processStep--imgContainer"><img alt="Students Submit Work" class="processStep--img" src="images/industries/communication.png"><div class="processStep--stepNumber">1</div></div><div class="processStep--textContainer"><div class="processStep--heading">Communication</div><div class="processStep--explanation"></div></div></div><div class="processStep"><div class="processStep--imgContainer"><img alt="Grade Submissions" class="processStep--img" src="images/industries/agriculture.png"><div class="processStep--stepNumber">2</div></div><div class="processStep--textContainer"><div class="processStep--heading">Agriculture</div><div class="processStep--explanation"></div></div></div><div class="processStep"><div class="processStep--imgContainer"><img alt="Send &amp; Export Grades" class="processStep--img" src="images/industries/energy.png"><div class="processStep--stepNumber">3</div></div><div class="processStep--textContainer"><div class="processStep--heading">Energy</div><div class="processStep--explanation"></div></div></div><div class="processStep"><div class="processStep--imgContainer"><img alt="Get Detailed Analytics" class="processStep--img" src="images/industries/healthcare.png"><div class="processStep--stepNumber">4</div></div><div class="processStep--textContainer"><div class="processStep--heading">Healthcare</div><div class="processStep--explanation"></div></div></div></div>
                    </div>
                    <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab" align="center">
                      <div class="processSteps processSteps-splash"><div class="processStep"><div class="processStep--imgContainer"><img alt="Students Upload Code" class="processStep--img" src="images/industries/fraud.png"><div class="processStep--stepNumber">1</div></div><div class="processStep--textContainer"><div class="processStep--heading">Fraud Detection Services</div><div class="processStep--explanation"></div></div></div><div class="processStep"><div class="processStep--imgContainer"><img alt="Grade Submissions" class="processStep--img" src="images/industries/automated.png"><div class="processStep--stepNumber">2</div></div><div class="processStep--textContainer"><div class="processStep--heading">Automated Banking</div><div class="processStep--explanation"></div></div></div><div class="processStep"><div class="processStep--imgContainer"><img alt="Send &amp; Export Grades" class="processStep--img" src="images/industries/datasecurity.jpg"><div class="processStep--stepNumber">3</div><div class="processStep--explanation"></div></div><div class="processStep--textContainer"><div class="processStep--heading">Data Security</div></div></div>
                  </div> </div> </div> </div>
                
         		</div>
 
         		</div>
         		</div>

    -->



<!-- 

<style>

nav > .nav.nav-tabs{

  border: none;
    color:#fff;
    background:#000;
    border-radius:0;
    align-items: center;

}
nav > div a.nav-item.nav-link,
nav > div a.nav-item.nav-link.active
{
  border: none;
    padding: 18px 25px;
    color:#fff;
    background:#000;
    border-radius:0;
}

nav > div a.nav-item.nav-link.active:after
 {
  content: "";
  position: relative;
  bottom: -60px;
  /*left: -10%;*/
  border: 15px solid transparent;
  }
.tab-content{
	align-content: left;
  background: #fdfdfd;
    line-height: 25px;
    /*border: 1px solid #ddd;*/
   /* border-top:5px solid #e74c3c;*/
   /* border-bottom:5px solid #e74c3c;*/
    padding:30px 25px;
}

nav > div a.nav-item.nav-link:hover,
nav > div a.nav-item.nav-link:focus
{
  border: none;
    background: #f3f3f3;
    color:#000;
    border-radius:0;
    transition:background 0.20s linear;
}
</style> -->




<div class="container"><h1>Bootstrap  tab panel example (using nav-pills)  </h1></div>
<div id="exTab1" class="container"> 
<ul  class="nav nav-pills">
      <li class="active">
        <a  href="#1a" data-toggle="tab">Education</a>
      </li>
      <li><a href="#2a" data-toggle="tab">Infrastructure</a>
      </li>
      <li><a href="#3a" data-toggle="tab">Finance</a>
      </li>
     <!--  <li><a href="#4a" data-toggle="tab">Background color</a>
      </li> -->
    </ul>

      <div class="tab-content clearfix">
        <div class="tab-pane active" id="1a">
         <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                     <div class="processSteps processSteps-splash"><div class="processStep"><div class="processStep--imgContainer"><img alt="Scan Student Work" class="processStep--img" src="images/industries/games.png"><div class="processStep--stepNumber">1</div></div><div class="processStep--textContainer"><div class="processStep--heading">Educational Games for Children</div><div class="processStep--explanation"></div></div></div><div class="processStep"><div class="processStep--imgContainer"><img alt="Grade Submissions" class="processStep--img" src="images/industries/lms.png"><div class="processStep--stepNumber">2</div></div><div class="processStep--textContainer"><div class="processStep--heading">Learning Management Systems</div><div class="processStep--explanation"></div></div></div><div class="processStep"><div class="processStep--imgContainer"><img alt="Send &amp; Export Grades" class="processStep--img" src="images/industries/dc.png"><div class="processStep--stepNumber">3</div></div><div class="processStep--textContainer"><div class="processStep--heading">Digital Classrooms</div><div class="processStep--explanation"></div></div></div><div class="processStep"><div class="processStep--imgContainer"><img alt="Get Detailed Analytics" class="processStep--img" src="images/industries/ct.png"><div class="processStep--stepNumber">4</div></div><div class="processStep--textContainer"><div class="processStep--heading">Corporate Training Materials</div><div class="processStep--explanation"></div></div></div></div>
                    </div>
        </div>
        <div class="tab-pane" id="2a">
          <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab" align="center">
                    <div class="processSteps processSteps-splash"><div class="processStep"><div class="processStep--imgContainer"><img alt="Students Submit Work" class="processStep--img" src="images/industries/communication.png"><div class="processStep--stepNumber">1</div></div><div class="processStep--textContainer"><div class="processStep--heading">Communication</div><div class="processStep--explanation"></div></div></div><div class="processStep"><div class="processStep--imgContainer"><img alt="Grade Submissions" class="processStep--img" src="images/industries/agriculture.png"><div class="processStep--stepNumber">2</div></div><div class="processStep--textContainer"><div class="processStep--heading">Agriculture</div><div class="processStep--explanation"></div></div></div><div class="processStep"><div class="processStep--imgContainer"><img alt="Send &amp; Export Grades" class="processStep--img" src="images/industries/energy.png"><div class="processStep--stepNumber">3</div></div><div class="processStep--textContainer"><div class="processStep--heading">Energy</div><div class="processStep--explanation"></div></div></div><div class="processStep"><div class="processStep--imgContainer"><img alt="Get Detailed Analytics" class="processStep--img" src="images/industries/healthcare.png"><div class="processStep--stepNumber">4</div></div><div class="processStep--textContainer"><div class="processStep--heading">Healthcare</div><div class="processStep--explanation"></div></div></div></div>
                    </div>
        </div>
        <div class="tab-pane" id="3a">
         <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab" align="center">
                      <div class="processSteps processSteps-splash"><div class="processStep"><div class="processStep--imgContainer"><img alt="Students Upload Code" class="processStep--img" src="images/industries/fraud.png"><div class="processStep--stepNumber">1</div></div><div class="processStep--textContainer"><div class="processStep--heading">Fraud Detection Services</div><div class="processStep--explanation"></div></div></div><div class="processStep"><div class="processStep--imgContainer"><img alt="Grade Submissions" class="processStep--img" src="images/industries/automated.png"><div class="processStep--stepNumber">2</div></div><div class="processStep--textContainer"><div class="processStep--heading">Automated Banking</div><div class="processStep--explanation"></div></div></div><div class="processStep"><div class="processStep--imgContainer"><img alt="Send &amp; Export Grades" class="processStep--img" src="images/industries/datasecurity.jpg"><div class="processStep--stepNumber">3</div><div class="processStep--explanation"></div></div><div class="processStep--textContainer"><div class="processStep--heading">Data Security</div></div></div>
                  </div> </div>
        </div>
         <!--  <div class="tab-pane" id="4a">
          <h3>We use css to change the background color of the content to be equal to the tab</h3>
        </div> -->
      </div>
  </div>



  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

  <link rel="stylesheet" type="text/css" href="css/industry_new.css" />