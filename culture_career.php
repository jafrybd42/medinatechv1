<link rel="stylesheet" type="text/css" href="css/development.css">
<script type='text/javascript' src='js/jquery.js'></script>
<script type='text/javascript' src='js/jquery2.js'></script>
<script type='text/javascript' src='js/jquery3.js'></script>

<script type='text/javascript' src='js/jquery4.js'></script>
<br/>
<br/>
<br/>
<br/>
<br/>
<div class="row">

               <div class="col-xl-6 mx-auto text-center">
                  <div class="section-title mb-100">
                     
                     <h4>Culture & Career Development</h4>
                  </div>
               </div>
            </div>
            <br/>
<br/>
<br/>
<br/>
<div class="vc_row-full-width vc_clearfix"></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper"><div class="feature-sections-wrapper ">
<div class="vc_column-inner"><div class="wpb_wrapper"><div class="feature-sections-wrapper ">
            
            <section id="kd-fss-5e55d890db819" class="featured-right ">
              
              <div class="container">
                <div class="side-content-wrapper"><img src="images/development/new_product.png" height="45"> <h3 class="side-content-title">Our Development Process</h3><div class="side-content-text"><p></p>
<ul>
<li>Understanding a new product</li>
<li>Appraisal for an existing product</li>
<li>Business analysis and design</li>
<li>Product development</li>
<li>DevOps and QA</li>
</ul>
<p></p></div>
<div class="side-content-link"><a href="development_process.php" target="_self" title="Read more" class="tt_button tt_secondary_button btn_primary_color">Read more</a></div></div>
<div class="side-featured-wrapper">
      <div class="featured-image"><img src="images/development/develop.png" class="attachment-full" alt=""></div>
    </div></div>
            </section><section id="kd-fss-5e55d890dc02c" class="featured-left ">
              
              <div class="container">
                <div class="side-content-wrapper"><img src="images/development/existing.png" height="45"> <h3 class="side-content-title">Join Us</h3><div class="side-content-text"><p></p>
                  <p>Here are some things we can propose you</p>
<ul>
<li><strong>Trainings </strong></li>
<li><strong>Remote work </strong></li>
<li><strong>Flexible hours </strong></li>
</ul>
<p></p></div>
<div class="side-content-link"><a href="join_us.php" target="_self" title="Read more" class="tt_button tt_secondary_button btn_primary_color">Read more</a></div></div>
<div class="side-featured-wrapper">
      <div class="featured-image"><img src="images/development/join.png" class="attachment-full" alt=""></div>
    </div></div>
            </section>
            </div></div></div>
          </div>
        </div>
      </div>
    </div>
  </div>
