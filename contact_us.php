<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Contact Page ---------->

<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,800,900%7cRaleway:300,400,500,600,700" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/contact_us.css">
      <!--Google map-->

    <section class="contact pt-100 pb-100" id="contact">
         <div class="container">

            <!-- <div class="row">

               <div class="col-xl-6 mx-auto text-center">
                  <div class="section-title mb-100">
                     
                     <h4>Contact with Us</h4>
                  </div>
               </div>
            </div> -->
            <!--Google map-->
<div class="mapouter"><div class="gmap_canvas" ><iframe height="300" width="100%" id="gmap_canvas" src="https://maps.google.com/maps?q=medina%20tech&t=k&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://ytify.com">ytify.com</a></div><style>.mapouter{position:relative;text-align:right;height:184px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:184px;width:100%;}</style></div>
            <div class="row text-center">
                  <div class="col-md-8">
                     <form action="#" class="contact-form">
                        <div class="row">
                           <div class="col-xl-6">
                              <input type="text" placeholder="name">
                           </div>
                           <div class="col-xl-6">
                              <input type="text" placeholder="email">
                           </div>
                           <div class="col-xl-6">
                              <input type="text" placeholder="subject">
                           </div>
                           <div class="col-xl-6">
                              <input type="text" placeholder="telephone">
                           </div>
                           <div class="col-xl-12">
                              <textarea placeholder="message" cols="30" rows="6"></textarea>
                              <input type="submit" value="Send Message">
                           </div>
                        </div>
                     </form>
                  </div>
                  <div class="col-md-4">
                     <div class="single-contact">
                        <i class="fa fa-map-marker"></i>
                        <h5>Address</h5>
                        <p>House #25, Road #4, Block #F, <br/>
                              Banani Dhaka,<br/>
                                 Dhaka 1213</p>
                     </div>
                     <div class="single-contact">
                        <i class="fa fa-phone"></i>
                        <h5>Phone</h5>
                        <p>+88 0963 8600 700</p>
                     </div>
                     <div class="single-contact">
                        <i class="fa fa-envelope"></i>
                        <h5>Email</h5>
                        <p>support@medinatech.co</p>
                     </div>
                  </div>
            </div>
         </div>
      </section>


</body>