


$(document).ready(function(){
 
 function fetch_post_data(id)
 {
  $.ajax({
   url:"save.php",
   method:"POST",
   data:{post_id:id},
   success:function(data)
   {
    $('#post_modal').modal('show');
    $('#post_detail').html(data);
   }
  });
 }

 $(document).on('click', '.view', function(){
  var id = $(this).attr("id");
  fetch_post_data(id);
 });

 $(document).on('click', '.previous', function(){
  var id = $(this).attr("id");
  fetch_post_data(id);
 });

 $(document).on('click', '.next', function(){
  var id = $(this).attr("id");
  fetch_post_data(id);
 });
 
});


