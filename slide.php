<?php
// session_start();
include('includes/header.php'); 
include('includes/navbar.php'); 


?>

<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
      <?php 
    include "head.php";
include('includes/scripts.php');

  ?>
    <!-- <h1 class="h3 mb-0 text-gray-800">Dashboard</h1> -->
    <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
        class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
  </div>
  <div class="splashSection--row splashSection--row-fullWidth js-assignmentStepsTabsContainer" align="center">


<link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

<!-- 	<script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
<!------ Include the above in your HEAD tag ---------->

 <div class="container">
              <div class="row" align="center">
                <div class="col-xs-12" align="center">
                  <nav align="center">
				  <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist" align="center">
                      <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Education</a>
                      <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Infrastructure</a>
                      <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Finance</a>
                     
                    </div>
                  </nav>
              
                  <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent" align="center">
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                     <div class="processSteps processSteps-splash"><div class="processStep"><div class="processStep--imgContainer"><img alt="Scan Student Work" class="processStep--img" src="images/industries/games.png"><div class="processStep--stepNumber">1</div></div><div class="processStep--textContainer"><div class="processStep--heading">Educational Games for Children</div><div class="processStep--explanation"></div></div></div><div class="processStep"><div class="processStep--imgContainer"><img alt="Grade Submissions" class="processStep--img" src="images/industries/lms.png"><div class="processStep--stepNumber">2</div></div><div class="processStep--textContainer"><div class="processStep--heading">Learning Management Systems</div><div class="processStep--explanation"></div></div></div><div class="processStep"><div class="processStep--imgContainer"><img alt="Send &amp; Export Grades" class="processStep--img" src="images/industries/dc.png"><div class="processStep--stepNumber">3</div></div><div class="processStep--textContainer"><div class="processStep--heading">Digital Classrooms</div><div class="processStep--explanation"></div></div></div><div class="processStep"><div class="processStep--imgContainer"><img alt="Get Detailed Analytics" class="processStep--img" src="images/industries/ct.png"><div class="processStep--stepNumber">4</div></div><div class="processStep--textContainer"><div class="processStep--heading">Corporate Training Materials</div><div class="processStep--explanation"></div></div></div></div>
                    </div>
                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab" align="center">
                    <div class="processSteps processSteps-splash"><div class="processStep"><div class="processStep--imgContainer"><img alt="Students Submit Work" class="processStep--img" src="images/industries/communication.png"><div class="processStep--stepNumber">1</div></div><div class="processStep--textContainer"><div class="processStep--heading">Communication</div><div class="processStep--explanation"></div></div></div><div class="processStep"><div class="processStep--imgContainer"><img alt="Grade Submissions" class="processStep--img" src="images/industries/agriculture.png"><div class="processStep--stepNumber">2</div></div><div class="processStep--textContainer"><div class="processStep--heading">Agriculture</div><div class="processStep--explanation"></div></div></div><div class="processStep"><div class="processStep--imgContainer"><img alt="Send &amp; Export Grades" class="processStep--img" src="images/industries/energy.png"><div class="processStep--stepNumber">3</div></div><div class="processStep--textContainer"><div class="processStep--heading">Energy</div><div class="processStep--explanation"></div></div></div><div class="processStep"><div class="processStep--imgContainer"><img alt="Get Detailed Analytics" class="processStep--img" src="images/industries/healthcare.png"><div class="processStep--stepNumber">4</div></div><div class="processStep--textContainer"><div class="processStep--heading">Healthcare</div><div class="processStep--explanation"></div></div></div></div>
                    </div>
                    <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab" align="center">
                      <div class="processSteps processSteps-splash"><div class="processStep"><div class="processStep--imgContainer"><img alt="Students Upload Code" class="processStep--img" src="images/industries/fraud.png"><div class="processStep--stepNumber">1</div></div><div class="processStep--textContainer"><div class="processStep--heading">Fraud Detection Services</div><div class="processStep--explanation"></div></div></div><div class="processStep"><div class="processStep--imgContainer"><img alt="Grade Submissions" class="processStep--img" src="images/industries/automated.png"><div class="processStep--stepNumber">2</div></div><div class="processStep--textContainer"><div class="processStep--heading">Automated Banking</div><div class="processStep--explanation"></div></div></div><div class="processStep"><div class="processStep--imgContainer"><img alt="Send &amp; Export Grades" class="processStep--img" src="images/industries/datasecurity.jpg"><div class="processStep--stepNumber">3</div><div class="processStep--explanation"></div></div><div class="processStep--textContainer"><div class="processStep--heading">Data Security</div></div></div>
                  </div> </div> </div> </div>
                
         		</div>
         		</div>
       </div>
   </div>
   

  <!--             </div>
        </div>
 -->

<style>

nav > .nav.nav-tabs{

  border: none;
    color:#fff;
    background:#000;
    border-radius:0;
    align-items: center;

}
nav > div a.nav-item.nav-link,
nav > div a.nav-item.nav-link.active
{
  border: none;
    padding: 18px 25px;
    color:#fff;
    background:#000;
    border-radius:0;
}

nav > div a.nav-item.nav-link.active:after
 {
  content: "";
  position: relative;
  bottom: -60px;
  /*left: -10%;*/
  border: 15px solid transparent;
  }
.tab-content{
	align-content: left;
  background: #fdfdfd;
    line-height: 25px;
    /*border: 1px solid #ddd;*/
   /* border-top:5px solid #e74c3c;*/
   /* border-bottom:5px solid #e74c3c;*/
    padding:30px 25px;
}

nav > div a.nav-item.nav-link:hover,
nav > div a.nav-item.nav-link:focus
{
  border: none;
    background: #f3f3f3;
    color:#000;
    border-radius:0;
    transition:background 0.20s linear;
}
</style>