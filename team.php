<!-- TEAM MEMBERS -->
        <div class="container" align="center">
           <section>
                <div class="rowt">
                    
                    <div class="columnt" style="width:30%" align="right">
                        <p class="heading" style="margin-left: 31px;margin-top: 100px;font-size:50px;">Our Team</p>
                    </div><div class="columnt" style="width:70%" align="left">
                        <div class="teamt">
                            <div class="team_title" style="padding-left: 45px;margin-left: -60px;">

                                <div class="rowt">
                                    <div class="columnt" align="left">
                                        <!--<p style="padding-top: 10px; align-content: left; padding-left: 15px;"> Medina Ali</p>-->

                                        <p style="margin-top: 10px; color:#D4AF37; margin-bottom :10px"><font style="font: Bold 20px/50px Poppins; align-content: left; padding-left: 15px;" color="#D4AF37">CEO</font> </p>
                                        <p style="margin-right: -30px;margin-left: -20px;color: black ; font:Italic 15px/25px Poppins; padding-top: 15px" align="left">
                                            “if you are not willing to sit with uncertainty, you will kill possibility” <br> <small>- TEDx Talks Keren Eldad</small>
                                        </p>
                                        <br style="padding-top: 15px">
                                       
                                        <!--<a href="https://www.linkedin.com/in/medinaali/" target="_blank"><img src="images/linkedin.png" style="padding-left: 15px" align="left"></a>-->
                                        <!--<a href="https://twitter.com/medinaali_" target="_blank"><img src="images/twitter-sign.png" style="padding-left: 15px" align="left"></a>-->
                                    </div>
                                    <div class="columnt" align="left">
                                        <a href="#apply">
                                            <img src="images/team/medina-mam.png" alt="CEO" style="width:100%;margin-left: 30px;margin-top: -10px;" align="left">
                                        </a>
                                    </div>
                                </div>
                                <!--      
               <div class="team_content" >
           Content
        </div>-->

                            </div>
                        </div>
                    </div>
                </div>

                <!-- Our team 1st part end -->

                <div class="rowt">

                    <div class="columnt" style="width:70%" align="right">
                        <div class="teamtt">
                            <div class="team_title" style="padding-left: 45px">

                                <div class="rowt">
                                    
                                    <div class="columnt" align="left">
                                        <a href="#apply">
                                            <img src="images/avatar/mustakim.png" alt="Back-End" style="width:120%;margin-left: -50px;margin-top: -20px;" align="left">
                                        </a>
                                    </div>
                                    <div class="columnt" align="right">
                                        <!--<p style="padding-top: 13px; font-family: Poppins;padding-right: 15px"> Mustakim Hussain</p>-->

                                        <p style="margin-left: -50px;margin-top: 13px; color:#D4AF37; margin-bottom :10px"><font style="font: 15px/30px Poppins;padding-right: 13px;font-size: 15px;/*! margin-top: 0px; */" color="#D4AF37">Back-End &amp; DevOps</font> </p>
                                        <p style="margin-left: -99px;color: black ; font:Italic 13px/20px Poppins; padding-top: 15px; padding-right: 15px;margin-top: 41px;width:175px;" align="right">
                                            " When something is important enough, you do it even if the odds are not in your favor "  <br>  <small>- Elon Musk</small>
                                        </p>
                                        <br style="padding-top: 15px">
                                       
                                        <!--<a href="https://www.linkedin.com/in/mustakim-hussain-657022ba/" target="_blank"><img src="images/linkedin.png" style="padding-right: 15px; " align="right"></a>-->
                                        
                                    </div>
                                </div>
                                <!--      
               <div class="team_content" >
           Content
        </div>-->

                            </div>
                        </div>
                    </div>
                    <!-- <div class="columnt" align="right" style="width:30%" align="right">
    <p class="heading" style="margin-top: 5%" >Our Team</p>
  </div> -->
                    <div class="columnt" style="width:70%" align="left">
                        <div class="teamtt">
                            <div class="team_title" style="padding-left: 45px">

                                <div class="rowt">
                                    <div class="columnt" style="margin-left: -40px;
margin-top: 0px;" align="left">
                                        <!--<p style="padding-top: 20px; font-family: Poppins; padding-left: 15px"> Ariba Alam</p>-->

                                        <p style="margin-right: -30px;margin-top: 21px; color:#D4AF37; margin-bottom :10px"><font style="font-family: Poppins;

padding-left: 15px;

font-size: 15px;" color="#D4AF37">AI &amp; ML Specialist</font> </p>
                                        <p style="margin-right: -30px;color: black ; font:Italic 13px/20px Poppins; padding-top: 15px;margin-top: 39px;
margin-bottom: 18px;" align="left">
                                            " You cannot go back &amp; change the beginning, but you can start where you are and change the ending "  <br>  <small>- C.S.Lewis</small> 
                                        </p>
                                        <br style="padding-top: 15px">
                                       
                                        <!--<a href="https://www.linkedin.com/in/syeda-ariba-alam-67a072199/" target="_blank"><img src="images/linkedin.png" style="padding-left: 15px;margin-top: -17px;" align="left"></a>-->
                                    </div>
                                    <div class="columnt" align="left">
                                        <a href="#apply">
                                            <img src="images/avatar/ariba.png" alt="AI-ML" style="width:100%;margin-left: 10px;margin-top: -20px;" align="left">
                                        </a>
                                    </div>
                                </div>
                                <!--      
               <div class="team_content" >
           Content
        </div>-->

                            </div>
                        </div>
                    </div>
                </div>

                <!-- End -->
                <!-- Our team 2nd part end -->

                <div class="rowt">

                    <div class="columnt" style="width:70%" align="right">
                        <div class="teamtt">
                            <div class="team_title" style="padding-left: 45px">

                                <div class="rowt">
                                    
                                    <div class="columnt" align="left">
                                        <a href="#apply">
                                            <img src="images/avatar/nibras.png" alt="UI/UX" style="width:127%;margin-left: -70px;margin-top: -10px;" align="left">
                                        </a>
                                    </div>
                                    <div class="columnt" align="right">
                                        <!--<p style="padding-top: 10px; font-family: Poppins; padding-right: 15px;margin-top: 2px;"> Nibras Khan</p>-->

                                        <p style="margin-left: -30px;margin-top: 10px; color:#D4AF37; margin-bottom :10px"><font style="font: 15px/30px Poppins; padding-right: 13px;font-size: 15px;" color="#D4AF37">UI/UX Developer</font> </p>
                                        <p style="color: black ; font:Italic 13px/20px Poppins; padding-top: 15px; padding-right: 15px;margin-top: 35px;width: 110%;margin-left: -70px;" align="right">
                                            " If work isn't fun, you're not playing on the right team "  <br>  <small>- Frank Sonenbarg</small>
                                        </p>
                                        <br style="padding-top: 20px">
                                      <br>
                                        <!--<a href="https://www.linkedin.com/in/nibraskhan35/" target="_blank"><img src="images/linkedin.png" style="padding-right: 15px;padding-top: 20px;margin-top: -32px;" align="right"></a>-->
                                    </div>
                                </div>
                                <!--      
               <div class="team_content" >
           Content
        </div>-->

                            </div>
                        </div>
                    </div>
                    <!-- <div class="columnt" align="right" style="width:30%" align="right">
    <p class="heading" style="margin-top: 5%" >Our Team</p>
  </div> -->
                    <div class="columnt" style="width:70%" align="left">
                        <div class="teamtt">
                            <div class="team_title" style="padding-left: 45px">

                                <div class="rowt">
                                    <div class="columnt" style="margin-left: -40px;
margin-top: 10px;" align="left">
                                        <!--<p style="padding-top: 10px; font-family: Poppins; padding-left:15px "> Kowshik Mazumder</p>-->

                                        <p style="margin-right: -30px;margin-top: 10px; color:#D4AF37; margin-bottom :10px"><font style=" padding-left: 15px;font-family: Poppins;

padding-left: 15px;

font-size: 15px;" color="#D4AF37">AI &amp; ML Specialist</font> </p>
                                        <p style="color: black ; font:Italic 13px/20px Poppins; padding-top: 15px;margin-top: 35px;margin-left: 0px;" align="left">
                                            " Insanity : doing the same thing over and over again and expecting different results. "<br><small>- Albert Einstein</small>
                                        </p>
                                        <br style="padding-top: 15px">
                                       
                                        <!--<a href="https://www.linkedin.com/in/kowshik-majumder-3687b6133/" target="_blank"><img src="images/linkedin.png" style="padding-left: 15px;/*! margin-top: -20px; */"></a>-->
                                    </div>
                                    <div class="columnt" align="left">
                                        <a href="#apply">
                                            <img src="images/avatar/kowshik.png" alt="AI-ML" style="width:120%;margin-left: -1px;margin-top: -15px;" align="left">
                                        </a>
                                    </div>
                                </div>
                                <!--      
               <div class="team_content" >
           Content
        </div>-->

                            </div>
                        </div>
                    </div>
                </div>

                <!-- End -->

                <!-- Our team 3rd part end -->

                <div class="rowt">

                    <div class="columnt" style="width:70%" align="right">
                        <div class="teamtt">
                            <div class="team_title" style="padding-left: 45px">

                                <div class="rowt">
                                    
                                    <div class="columnt" align="left">
                                        <a href="#apply">
                                            <img src="images/avatar/jafry.png" alt="Front-End" style="width:120%;margin-left: -70px;margin-top: -9px;" align="left">
                                        </a>
                                    </div>
                                    <div class="columnt" align="right">
                                        <!--<p style="padding-top: 10px; font-family: Poppins;padding-right: 15px;margin-top: 3px;"> Jafry Deep</p>-->

                                        <p style="margin-top: 10px; color:#D4AF37; margin-bottom :10px; padding-right: 15px;margin-left: -61px;"><font style="font: 15px/30px Poppins; font-size : 15px; margin-top: -0px;" color="#D4AF37">Front-End Developer</font> </p>
                                        <p style="margin-left: -67px;color: black ; font:Italic 13px/20px Poppins; padding-top: 15px;padding-right: 15px;margin-top: 35px;/*! width: 0px; */" align="right">
                                            " A winner is a Dreamer who Never gives up. "<br><small>- Nelson Mandela</small>
                                        </p>
                                        <br style="padding-top: 15px">
                                       
                                        <!--<a href="https://www.linkedin.com/in/jafry-deep-a926b6160/" target="_blank"><img src="images/linkedin.png" style="padding-top: 20px;padding-right: 15px"></a>-->
                                    </div>
                                </div>
                                <!--      
               <div class="team_content" >
           Content
        </div>-->

                            </div>
                        </div>
                    </div>
                    <!-- <div class="columnt" align="right" style="width:30%" align="right">
    <p class="heading" style="margin-top: 5%" >Our Team</p>
  </div> -->
                    <div class="columnt" style="width:70%" align="left">
                        <div class="teamtt">
                            <div class="team_title" style="padding-left: 45px">

                                <div class="rowt">
                                    <div class="columnt" style="margin-left: -40px;
margin-top: 10px;" align="left">
                                        <!--<p style="padding-top: 10px ; font-family: Poppins ; padding-left: 15px"> Abu Bakkar Shiddique</p>-->

                                        <p style="margin-right: -30px;margin-top: 10px; color:#D4AF37; margin-bottom :10px"><font style="font-family: Poppins;

padding-left: 15px;

font-size: 15px;" color="#D4AF37"> Research Specialist</font> </p>
                                        <p style="margin-right: -30px;color: black ; font:Italic 13px/20px Poppins; padding-top: 15px;padding-left: 15px; margin-top: 35px;" align="left">
                                            " The meaning of life is to fall seven times and get up at eighth "<br><small>- Paulo Coelho</small>
                                        </p>
                                        <br style="padding-top: 15px">
                                       
                                        <!--<a href="https://www.linkedin.com/in/md-abubakkar-shiddique-shovo/" target="_blank"><img src="images/linkedin.png" style="padding-left: 15px"></a>-->
                                    </div>
                                    <div class="columnt" align="left">
                                        <a href="#apply">
                                            <img src="images/avatar/shuvo.png" alt="TRA" style="width:120%; padding-left: 15px;margin-left: -16px;margin-top: -20px;" align="left">
                                        </a>
                                    </div>
                                </div>
                                <!--      
               <div class="team_content" >
           Content
        </div>-->

                            </div>
                        </div>
                    </div>
                </div>
                <!--Start 4th Part-->
                <div class="rowt">

                    <div class="columnt" style="width:70%" align="right">
                        <div class="teamtt">
                            <div class="team_title" style="padding-left: 45px;font: 14px/30px Poppins;">

                                <div class="rowt">
                                    
                                    <div class="columnt" align="left">
                                        <a href="#apply">
                                            <img src="images/team/ashraful.png" alt="Ashraful" style="width:120%;margin-left: -70px;margin-top: -6px;" align="left">
                                        </a>
                                    </div>
                                    <div class="columnt" align="right">
                                        <!--<p style="padding-top: 10px; font-family: Poppins;padding-right: 15px;margin-top: 3px;"> Ashraful Islam Sheiblu</p>-->

                                        <p style="margin-left: -50px;margin-top: 10px; color:#D4AF37; margin-bottom :10px; padding-right: 15px;"><font style="font: 15px/30px Poppins; font-size : 15px; margin-top: -0px;" color="#D4AF37">Full Stack Developer</font> </p>
                                        <p style="margin-left: -80px;color: black ; font:Italic 13px/20px Poppins; padding-top: 15px;padding-right: 15px;margin-top: 35px;/*! width: 0px; */" align="right">
                                            " Do what you can, with what you have, where you are "<br><small>- Theodore Roosevelt</small>
                                        
                                        </p>
                                        <br style="padding-top: 15px">
                                       
                                        <!--<a href="https://www.linkedin.com/in/ashraful-islam-sheiblu-4a96a0b9/" target="_blank"><img src="images/linkedin.png" style="padding-top: 0px;padding-right: 15px"></a>-->
                                    </div>
                                </div>
                                <!--      
               <div class="team_content" >
           Content
        </div>-->

                            </div>
                        </div>
                    </div>
                 
                    <div class="columnt" style="width:70%" align="left">
                        <div class="teamtt">
                            <div class="team_title" style="padding-left: 45px;font: 14px/30px Poppins;">

                                <div class="rowt">
                                    <div class="columnt" style="margin-left: -40px;
margin-top: 10px;" align="left">
                                        <!--<p style="padding-top: 10px ; font-family: Poppins ; padding-left: 15px">Shovon Lal Chakraborty</p>-->

                                        <p style="margin-right: -35px;margin-top: 10px; color:#D4AF37; margin-bottom :10px"><font style="font-family: Poppins;

padding-left: 15px;

font-size: 15px;" color="#D4AF37"> Full Stack Developer</font> </p>
                                        <p style="color: black ; font:Italic 13px/20px Poppins; padding-top: 15px;padding-left: 15px; margin-top: 35px;" align="left">
                                            " Know Thyself " <br>  <small>- Socrates</small>
                                        </p>
                                        <br style="padding-top: 15px">
                                       
                                        <!--<a href="/" target="_blank" ><img src="images/linkedin.png" style="margin-top: 40px;" style="padding-left: 15px;"></a>-->
                                    </div>
                                    <div class="columnt" align="left">
                                        <a href="#apply">
                                            <img src="images/team/shovon.png" alt="TRA" style="width:120%; padding-left: 20px;margin-left: -5px;margin-top: -13px;" align="left">
                                        </a>
                                    </div>
                                </div>
                             

                            </div>
                        </div>
                    </div>
                </div>
                <!--Start 5th Part-->
                <div class="rowt">

                    <div class="columnt" style="width:70%" align="right">
                        <div class="teamtt">
                            <div class="team_title" style="padding-left: 45px;font: 14px/30px Poppins;">

                                <div class="rowt">
                                    
                                    <div class="columnt" align="left">
                                        <a href="#apply">
                                            <img src="images/team/shahnewaz.png" alt="Md. Shahnawaz Hossain" style="width:120%;margin-left: -70px;margin-top: -20px;" align="left">
                                        </a>
                                    </div>
                                    <div class="columnt" align="right">
                                        <!--<p style="padding-top: 10px; font-family: Poppins;padding-right: 15px;margin-top: 3px;"> Ashraful Islam Sheiblu</p>-->

                                        <p style="margin-top: 10px; color:#D4AF37; margin-bottom :10px; padding-right: 15px;"><font style="font: 15px/30px Poppins; font-size : 15px; margin-top: -0px;" color="#D4AF37">AI / ML Intern</font> </p>
                                       <p style="margin-left: -75px;color: black ; font:Italic 12px/20px Poppins; padding-top: 15px;padding-right: 15px;margin-top: 37px;" align="right">
                                            " To be yourself in the world, where things nearby trying to make you involved in anything else is the best accomplishment, I guess, being a user-defined homo sapiens.  "<br><small>- Yuval Harari</small>
                                        </p>
                                        <br style="padding-top: 15px">
                                       
                                        <!--<a href="https://www.linkedin.com/in/ashraful-islam-sheiblu-4a96a0b9/" target="_blank"><img src="images/linkedin.png" style="padding-top: 0px;padding-right: 15px"></a>-->
                                    </div>
                                </div>
                                <!--      
               <div class="team_content" >
           Content
        </div>-->

                            </div>
                        </div>
                    </div>
                 
                    <div class="columnt" style="width:70%" align="left">
                        <div class="teamtt">
                            <div class="team_title" style="padding-left: 45px;font: 14px/30px Poppins;">

                                <div class="rowt">
                                    <div class="columnt" style="margin-left: -40px;
margin-top: 10px;" align="left">
                                        <!--<p style="padding-top: 10px ; font-family: Poppins ; padding-left: 15px">Shovon Lal Chakraborty</p>-->

                                        <p style="margin-right: -30px;margin-left: -10px;margin-top: 10px; color:#D4AF37; margin-bottom :10px"><font style="font-family: Poppins;

padding-left: 15px;

font-size: 15px;" color="#D4AF37">  Financial planning &</font> </p>
<p style="margin-right: -30px;
margin-left: -10px;margin-top: -16px; color:#D4AF37; margin-bottom :10px"><font style="font-family: Poppins;

padding-left: 15px;

font-size: 15px;" color="#D4AF37"> Analysis Manager</font> </p>
                                        <p style="color: black ; font:Italic 13px/20px Poppins; padding-top: 15px;padding-left: 15px; margin-top: 10px;" align="left">
                                            " The way to get started is to quit talking and begin doing " <br>  <small>- Walt Disney</small>
                                        </p>
                                        <br style="padding-top: 15px">
                                       
                                        <!--<a href="/" target="_blank" ><img src="images/linkedin.png" style="margin-top: 40px;" style="padding-left: 15px;"></a>-->
                                    </div>
                                    <div class="columnt" align="left">
                                        <a href="#apply">
                                            <img src="images/team/tan.png" alt="TRA" style="width:110%; padding-left: 15px;margin-left: 7px;margin-top: -30px;" align="left">
                                        </a>
                                    </div>
                                </div>
                             

                            </div>
                        </div>
                    </div>
                </div>
                <!-- End -->
<!--Start 6th Part-->
                <div class="rowt" style="width: 50%;margin-left: -50%;">

                    <div class="columnt" style="width:70%" align="right">
                        <div class="teamtt">
                            <div class="team_title" style="padding-left: 45px;font: 14px/30px Poppins;">

                                <div class="rowt">
                                    
                                    <div class="columnt" align="left">
                                        <a href="#apply">
                                            <img src="../images/avatar/ben.png" alt="HR" style="width: 65%;margin-left: -30px;margin-top: 2px;" align="left">
                                        </a>
                                    </div>
                                    <div class="columnt" align="right">
                                        <!--<p style="padding-top: 10px; font-family: Poppins;padding-right: 15px;margin-top: 3px;"> Ashraful Islam Sheiblu</p>-->

                                        <p style="margin-top: 10px; color:#D4AF37; margin-bottom :10px; padding-right: 15px;"><font style="font: 15px/30px Poppins; font-size : 15px; margin-top: -0px;" color="#D4AF37">HR & Admin</font> </p>
                                       <p style="margin-left: -80px;color: black ; font:Italic 12px/20px Poppins; padding-top: 15px;padding-right: 15px;margin-top: 37px;width: 95%;" align="right">
                                            " In the practice of tolerance, one’s enemy is the best teacher.  "<br><small>- Dalai Lama</small>
                                        </p>
                                        <br style="padding-top: 15px">
                                       
                                        <!--<a href="https://www.linkedin.com/in/ashraful-islam-sheiblu-4a96a0b9/" target="_blank"><img src="images/linkedin.png" style="padding-top: 0px;padding-right: 15px"></a>-->
                                    </div>
                                </div>
                                <!--      
               <div class="team_content" >
           Content
        </div>-->

                            </div>
                        </div>
                    </div>
                 
<!--                    <div class="columnt" style="width:70%" align="left">-->
<!--                        <div class="teamtt">-->
<!--                            <div class="team_title" style="padding-left: 45px;font: 14px/30px Poppins;">-->

<!--                                <div class="rowt">-->
<!--                                    <div class="columnt" style="margin-left: -40px;-->
<!--margin-top: 10px;" align="left">-->
                                        <!--<p style="padding-top: 10px ; font-family: Poppins ; padding-left: 15px">Shovon Lal Chakraborty</p>-->

<!--                                        <p style="margin-top: 10px; color:#D4AF37; margin-bottom :10px"><font style="font-family: Poppins;-->

<!--padding-left: 15px;-->

<!--font-size: 15px;" color="#D4AF37">  Financial planning &</font> </p>-->
<!--<p style="margin-top: -16px; color:#D4AF37; margin-bottom :10px"><font style="font-family: Poppins;-->

<!--padding-left: 15px;-->

<!--font-size: 15px;" color="#D4AF37"> Analysis Manager</font> </p>-->
<!--                                        <p style="color: black ; font:Italic 13px/20px Poppins; padding-top: 15px;padding-left: 15px; margin-top: 10px;" align="left">-->
<!--                                            " The way to get started is to quit talking and begin doing " <br>  <small>- Walt Disney</small>-->
<!--                                        </p>-->
<!--                                        <br style="padding-top: 15px">-->
                                       
                                        <!--<a href="/" target="_blank" ><img src="images/linkedin.png" style="margin-top: 40px;" style="padding-left: 15px;"></a>-->
<!--                                    </div>-->
<!--                                    <div class="columnt" align="left">-->
<!--                                        <a href="#apply">-->
<!--                                            <img src="images/team/tan.png" alt="TRA" style="width:110%; padding-left: 15px;margin-left: -17px;margin-top: -30px;" align="left">-->
<!--                                        </a>-->
<!--                                    </div>-->
<!--                                </div>-->
                             

<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
                </div>
                <!-- End -->
            </section>
            <br>
            <br>
            <br>
        </div>
        
        
        