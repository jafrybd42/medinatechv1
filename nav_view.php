<!--favicons-->

<link rel="apple-touch-icon" sizes="57x57" href="images/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="images/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="images/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="images/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="images/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="images/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="images/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="images/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="images/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="images/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="images/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="images/favicon-16x16.png">
<!-- --------------------tab menu------------------------- -->

  <div class="tabMenu">

    <div>
      <a href="index.php"><img class="tabMenu_logo" src="../skins/MedinaTech PNG.png" alt="logo"></a>
    </div>

    <ul style="width: 95%;/*! margin-left: 10%; */">

        <li style="margin-left: 215px;"><a class="active" href="index.php"><b>Home Page</b></a></li>
        <li><a href="teams.php"  ><b>Culture &amp; Career Development</b></a></li>
        <li><a href="ReachUs_MT.php"><b>Reach Us</b></a></li>

      </ul>

  </div>

<!-- ---------------mobile menu---------------------------------- -->

  <div class="mobileMenu">

    <a href="index.php"><img style="width: 140px; position: absolute;padding: 2px 10px;" src="../skins/MedinaTech PNG.png" alt="logo"></a>

    <div class="dropdown">
      <img onclick="myFunction()" class="dropbtn" style="width: 25px" src="../skins/menu.png" alt="menu">
      
      <div id="myDropdown" class="dropdown-content">
        <a class="active" href="index.php">Home</a>
        <a href="teams.php">Culture & Career Development</a>
        <a href="ReachUs_MT.php">Reach Us</a>
      </div>
    </div>
  </div>

  <!-- ---------------pc navbar---------------------------------- -->

  <div class="navbar">
      
      <ul>

        <li><a href="ReachUs_MT.php"><b>Reach Us</b></a></li>
        <li><a href="teams.php"><b>Culture & Career Development</b></a></li>
        <li><a class="active" href="index.php"><b>Home Page</b></a></li>

      </ul>
      <a href="index.php"><img class="lionLogo" src="../skins/lion.png" alt="logo"></a>
    
  </div>

  <!-- ------------------------------------ -->
  <!--<div class="social">-->
  <!--  <a href="http://www.facebook.com/sharer/sharer.php?u=https://medinatech.co" target="_blank"><img src="../skins/fb.png" alt="logo"></a>-->
    
  <!--  <a href="https://www.linkedin.com/shareArticle?mini=true&url=https%3A%2F%2Fwww.medinatech.co&title=Medina+Tech" target="_blank"><img src="../skins/in.png" alt="logo">  </a>-->
  <!--</div>-->

  <!-- --------------------------------------- -->
