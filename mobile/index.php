<html>

<head>
    <!--Redirect to different pages based on Screen Size-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            if ($(window).width() >= 1000) {
                window.location = "../index.php";
            }
            if ($(window).width() > 750 && $(window).width() < 1000) {
                window.location = "../tab/index.php";
            }
        });
    </script>
    <!-- Adding Stylesheet and Favicon -->

    <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
    <link rel="apple-touch-icon" sizes="57x57" href="../images/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="../images/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../images/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="../images/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../images/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="../images/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="../images/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="../images/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="../images/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="../images/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="../images/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../images/favicon-16x16.png">
    <!-- <link rel="manifest" href="../manifest.json"> -->
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Medina Tech</title>
    <meta charset="utf-8" title="MedinaTech">

    <link href="https://fonts.googleapis.com/css?family=Poppins:600,700|Roboto&display=swap" rel="stylesheet">
    <!--PWA-->
    <link rel="manifest" href="./manifest.json">
    <script src="./index.js" type="module"></script>
    <meta name="mobile-web-app-capable" content="yes">

    <link rel="shortcut icon" sizes="192x192" href="../skins/512.png">
    <link rel="shortcut icon" sizes="128x128" href="../skins/128.png">
    <link rel="apple-touch-icon" sizes="128x128" href="../skins/128.png">
    <link rel="apple-touch-icon-precomposed" sizes="128x128" href="../skins/128.png">
    <!--CSS-->
    <link rel="stylesheet" href="../style_MT.css">
    <link rel="stylesheet" href="../Updated.css">
    <!--Scripts-->
    <script src="../functions_MT.js"></script>


</head>

<body>

    <!-- Adding Nav View For Mobile/PC/Tab -->
    <?php
    include "../nav_view.php";
    ?>
    <!-- Top View (Image,Text) -->
    <div class="gridFull">
        <div class="allItem">
            <section class="homepageHeaderSec">
                <div style="" class="grid1">

                    <div class="item1">
                        <img class="graph1" style="cursor: not-allowed;width: 100%;" src="../skins/FULL2.gif" alt="top image">
                        <img class="graph2" style="cursor: not-allowed;margin-right: auto; margin-left: 42px;
                                                           margin-right: auto;" src="../skins/FULL(mobile)2.gif" alt="top image">
                    </div>

                    <div class="item2">
                        <p class="bigText" style="font-family: Poppins;
                                                          font-size: 25px;
                                                          letter-spacing: 0;
                                                          color: #000000;
                                                          text-shadow: 5px 5px 6px #00000029;
                                                          opacity: 1;
                                                          font-weight: 600;
                                                         ">
                            We innovate
                            <br> We work together
                            <br> We solve problems
                            <br>
                        </p>

                        <p style="font-family: 'Roboto', sans-serif; font-size: 15px;">
              <br><br>
              Medina Tech is a Software Company, founded in February 2020, operating out of Dhaka Bangladesh. Medina Tech’s main focus is work on projects that uphold Technology for Social Good, therefore Medina Tech specializes in building Education Technology Solutions.
<br>Our  values entail to prioritize UI/UX, Accessibility, Security & proactive Customer Support for all software solutions.
              <br><br>
            </p>


                        <!-- Work With Us Linked -->



                        <a href="#" class="button" style="background-color: black; color: white; border-color: black; width: 150px; font-family: Poppins;text-align: center;
font: SemiBold 20px/30px Poppins;
letter-spacing: 0;
color: #FFFFFF;
opacity: 1;background: #000000 0% 0% no-repeat padding-box;
box-shadow: 3px 3px 6px #00000029;
opacity: 1;
cursor: not-allowed;"><b>Work with Us !</b></a>


                        <!-- Innovate with us Linked -->


                        <a href="#" class="button" style="background: transparent linear-gradient(117deg, #F7E014 0%, #B79D24 100%) 0% 0% no-repeat padding-box;
box-shadow: 3px 3px 6px #00000029;
opacity: 1; text-align: center;
font-family:  'Poppins';
border-color: #F7E014;
letter-spacing: 0;
color: #000000;
opacity: 1;
width: 150px;
cursor: not-allowed;
"><b>Innovate with Us !</b></a>
                    </div>
                </div>
            </section>



            <!-- -------HOW WE WORK SECTION------------------- -->

            <section style="padding-bottom: 20px;">
                <img style="cursor: not-allowed;width: 100%; right: 0px; z-index: -1" src="images/bg.png" alt="background">
                <p class="heading_new" style=" text-align: ;
font: Bold 80px/90px Poppins;
letter-spacing: 0;
color: #000000;
text-shadow: 5px 5px 6px #00000029;
opacity: 1;
font-size: 30px;
margin-top: -95px;
margin-start: -10px;
margin-left: 0px;
">How We Work ?</p>

                <div class="container">
                    <p style="
font-family:  Poppins;
letter-spacing: 0;
color: #000000;
opacity: 1;
font-size: 24px;
padding-top: 30px; " align="center"><b>
                            <font color="black">Have an Idea ? </font> <br>
                            <font color="#D7B722">Have a Problem ?</font>
                        </b>
                    <div class="grid2" style="padding-bottom: 100px;">
                        <div class="item3" style="margin-right: -30px;
margin-left: -20px;">
                            <p style="background: #000000 0% 0% no-repeat padding-box;
opacity: 1;
text-align: center;
font-family: Poppins;
font-weight: 600;
letter-spacing: 0;
color: #FFFFFF;
opacity: 1;
width: 168px;
height: 25px;
margin-left: 85px;
padding-top: 5px;

font: SemiBold Poppins;
letter-spacing: 0;
color: #FFFFFF;
opacity: 1;
margin-bottom: -17px;
font-size: 20px;
padding-bottom: 10px;
position: absolute;
margin-top: -7px;
" align="right">Solutions</p>
                            <br>
                            <p style="

          text-align: center;
font-family: Roboto;
letter-spacing: 0;
color: #303030;
font-size: 14px;
font-weight: 400;
opacity: 1; background: #FFFFFF 0% 0% no-repeat padding-box;
box-shadow: 0px 3px 15px #00000029;
border-radius: 5px;
opacity: 1; padding: 25px;
padding-top:35px;">Our team of engineers are here to help our clients create software solutions. If you belong to an industry that is a part of our concern, we will help you find the most optimal- manageable and updated solutions using latest tech stacks</p>
                        </div>
                        <div class="item4">
                            <img src="../skins/Group 5.png" style="cursor: not-allowed;" alt="Cover">
                        </div>
                        <div class="item5" style="margin-right: -30px;
margin-left: -20px;">
                            <p style="background: #D7B722 0% 0% no-repeat padding-box;
opacity: 1;
text-align: center;
font-family: Poppins;
font-weight: 600;
letter-spacing: 0;
color: #FFFFFF;
opacity: 1;
width: 168px;
height: 25px;
margin-left: 85px;
padding-top: 5px;

font: SemiBold Poppins;
letter-spacing: 0;
color: #FFFFFF;
opacity: 1;
margin-bottom: -17px;
font-size: 20px;
padding-bottom: 10px;
position: absolute;
margin-top: -7px;
" align="right">Innovations</p>
                            <br>
                            <p style="text-align: center;
font-family: Roboto;
font-weight: 400;
letter-spacing: 0;
color: #303030;
opacity: 1;
font-size: 14px;
background: #FFFFFF 0% 0% no-repeat padding-box;
box-shadow: 0px 3px 15px #00000029;
border-radius: 5px;
opacity: 1;
padding: 25px;
padding-top: 35px;">Our team of engineers are always researching to innovate by building specialized in-house products for our industries of concern. We also welcome, people with ideas to come to us & collaboratively innovate by using our resources.</p>
                        </div>

                    </div>

                </div>
            </section>
        </div>

        <!-- ----------INDUSTRY SECTION------------------ -->

        <section style="padding-bottom: 80px;
                padding-top: 20px;
                background: #f7f7f7;">
            <p class="heading" style="text-align: left;
font-weight: 600;
font-family: Poppins;
letter-spacing: 0;
color: #000000;
text-shadow: 5px 5px 6px #00000029;
opacity: 1;
font-size: 30px;
margin-top: -95px;">Projects
                <br>-
            </p>

            <!-- <div class="container" style="width: 100%; background: #f7f7f7" align="center">
                <div class="grid2" style="grid-template-areas: 'center';width: 100%; margin-left: -40px; padding: 20px;" align="center">

                    <div class="tab" align="center" style="margin-left: 45px;">
                        <button class="tablinks" onclick="openCity(event, 'London')" onclick="Bold()" id="defaultOpen" align="center"><img src="../images/book.png" alt="book" height="45px" />
                            <br>
                            <br>Education</button>
                        <button class="tablinks" onclick="openCity(event, 'Paris')" onclick="Bold()" id="texto" align="center"><img id="imgg" src="../images/Infrastructure.png" alt="infr" height="45px" />
                            <br>
                            <br>Infrastructure</button>
                        <button class="tablinks" onclick="openCity(event, 'Tokyo')" onclick="Bold()" align="center"><img src="../images/Finance.png" alt="finance" height="45px" />
                            <br>
                            <br>Finance</button>
                    </div>

                    <div id="London" class="tabcontent" style="width: 90%;">

                        <img src="images/ed_1.png" alt="education" align="center" class="industry_img" style="cursor: not-allowed;"><br />
                        <img src="images/ed_2.png" alt="education" align="center" class="industry_img" style="cursor: not-allowed;">
                    </div>

                    <div id="Paris" class="tabcontent" style="width: 90%;">

                        <img src="images/infr_1.png" alt="industry" align="center" class="industry_img" style="cursor: not-allowed;"><br />
                        <img src="images/infr_2.png" alt="industry" align="center" class="industry_img" style="cursor: not-allowed;">
                    </div>

                    <div id="Tokyo" class="tabcontent" style="width: 90%;">

                        <img src="images/fin.png" alt="finance" align="center" class="industry_finance" style="cursor: not-allowed;">
                    </div>

                    <script>
                        function openCity(evt, cityName) {
                            var i, tabcontent, tablinks;
                            tabcontent = document.getElementsByClassName("tabcontent");
                            for (i = 0; i < tabcontent.length; i++) {
                                tabcontent[i].style.display = "none";
                            }
                            tablinks = document.getElementsByClassName("tablinks");
                            for (i = 0; i < tablinks.length; i++) {
                                tablinks[i].className = tablinks[i].className.replace(" active", "");
                                imgg.updateSrc = "images/book.png";
                            }
                            document.getElementById(cityName).style.display = "block";
                            evt.currentTarget.className += " active";

                        }

                        document.getElementById("defaultOpen").click();
                    </script>
                </div>

            </div> -->
            <div class="form_" style="padding:0px!important;width:100%;margin-top:15px;">
                <small class="dropline" style="font-size:15px;width:100%; text-align:center;">To know more about our projects or to request a demo <br>feel free to contact us</small>

                <div>
                    <form action="../reach_us_config.php" method="post">

                        <input class="formHalfBox" style="width: 90%;margin: 10px;" type="text" id="name" name="name" placeholder="Full Name">

                        <input class="formHalfBox" style="width: 90%;margin: 10px;" type="text" id="email" name="email" placeholder="E-mail"> <br>

                        <input class="formFullBox" style="width: 90%;margin: 10px;" type="text" id="subject" name="subject" placeholder="Subject">

                        <textarea class="formFullBox txtarea" style="width: 90%;margin: 10px;" id="message" rows="4" cols="50" name="message" placeholder="Your Message ..."></textarea><br>

                        <input type="submit" value="Send Message" style="display: block; margin: auto;">

                    </form>
                </div>

            </div>
        </section>

        <!-- Adding video and Text at the END -->

        <section>
            <div class="lastPortion" style="margin-top: -1%">

                <div class="ltext" style="text-align: center;
        ">
                    <p class="bigText" style="font-family: Poppins;
        font-weight: 600;

        letter-spacing: 0;
        opacity: 1;">“<span style="color:#D7B722">Code</span> speaks<br>
                        Louder than words”</p>

                    <p style="
      font-family: Roboto;
      font-weight: 400;
      font-size: 15px;
      letter-spacing: 0;
      color: #000000;
      opacity: 1;">
                        <br>
                        In Medina Tech, we aim to create a community along
                        with our projects & solutions, to enhance the Goals of
                        a Digital Bangladesh!
                    </p>
                </div>

                <div class="lvideo">


                    <video autoplay="" loop="" muted="" playsinline="" src="../details.mp4" style="cursor: not-allowed" type="video/mp4" style="background: #FFFFFF 0% 0% no-repeat padding-box;
      box-shadow: 0px 3px 20px #00000029;
    opacity: 1;"></video>


                </div>


            </div>
        </section>






        <!-- --------------------------Footer Section--------------------------------- -->


        <style type="text/css">
            .splashSection--videoContainer {
                width: 40px;
                height: 50%;
                -webkit-box-flex: 2;
                -ms-flex: 2;
                flex: 2;
                -ms-flex-preferred-size: auto;
                flex-basis: auto;
                box-shadow: 0 4px 16px 0 rgba(31, 51, 51, 0.1), 0 2px 8px 0 rgba(31, 51, 51, 0.2)
            }
        </style>

    </div>
    </div>
    <!--Footer Section-->
    <?php
    include "footer_new.php";
    //  include('includes/scripts.php');
    ?>

</body>

</html>