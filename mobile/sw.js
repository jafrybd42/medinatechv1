const staticCacheName = 'cache-v1.71';

const assets = [
  './',
  './images/bg.png',
  './images/ed_1.png',
  './images/ed_2.png',
  './images/fin.png',
  './images/Finance.png',
  './images/font@2x.png',
  './images/infr_1.png',
  './images/infr_2.png',
  './images/lion@2x.png',
  './images/mt_logo.png',
  './images/mt_text.png',
  './images/team.png',

  './footer_new.php',
  './index.js',
  './index.php',
  './ReachUs_MT.php',
  './section.php',
  './teams.php'
];

// install event
self.addEventListener('install', evt => {
  //console.log('service worker installed');
  evt.waitUntil(
    caches.open(staticCacheName).then((cache) => {
      console.log('caching shell assets');
      cache.addAll(assets);
    })
  );
});

// activate event
self.addEventListener('activate', evt => {
  //console.log('service worker activated');
  evt.waitUntil(
    caches.keys().then(keys => {
      //console.log(keys);
      return Promise.all(keys
        .filter(key => key !== staticCacheName)
        .map(key => caches.delete(key))
      );
    })
  );
});

// fetch event
self.addEventListener('fetch', evt => {
  //console.log('fetch event', evt);
  evt.respondWith(
    caches.match(evt.request).then(cacheRes => {
      return cacheRes || fetch(evt.request);
    })
  );
});