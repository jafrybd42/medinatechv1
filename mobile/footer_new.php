<!--Section Start-->
<section style="background: #000000; ">
    <!--Fonts-->
	  <link href="https://fonts.googleapis.com/css?family=Poppins:600,700|Roboto&display=swap" rel="stylesheet">
	  
	 
	<div class="container" style="background: #000000 0% 0% no-repeat padding-box;
opacity: 1; align-content: center;" align="center">
	<!--Contact Section -->
	<p style="text-align: center;
font: Bold 30px/46px Poppins;
letter-spacing: 0;
color: #FFFFFF;
opacity: 1;
padding-top: 50px;
padding-bottom: 20px;
font-weight:bold;">Contact </p>

<p style="text-align: center;
font-family:  Roboto;
letter-spacing: 0;
color: #FFFFFF;
opacity: 1;
padding-top: 20px;
padding-bottom: 30px;"> support@medinatech.com <br><br> +8809638600700 </p>

<a href="https://www.facebook.com/Medina-Tech-110625163812720/" target="_blank"><img src="../skins/fb - Copy.png" alt="logo" style="height: 20px; width: 20px; padding-right: 2.5px;" ></a>
<a href="https://www.linkedin.com/company/medina-tech/" target="_blank"><img src="../skins/in - Copy.png" alt="logo" style=" padding-left: 2.5px; height: 20px; width: 20px;"></a>


    <!--Location Section -->

	<p style="text-align: center;
font: Bold 30px/46px Poppins;
letter-spacing: 0;
color: #FFFFFF;
opacity: 1;
padding-top: 30px;
padding-bottom: 20px;
font-weight:bold;">Location </p>

<p style="text-align: center;
font-family:  Poppins;
letter-spacing: 0;
color: #FFFFFF;
opacity: 1;

border-bottom: 1px solid gold;
display: inline-block;
padding-top: 20px;
padding-bottom: 5px;
padding-right: 50px;
padding-left: 50px;"> Bangladesh  </p>


<p style="text-align: center;
font-family:  Roboto;
letter-spacing: 0;
color: #FFFFFF;
opacity: 1;
padding-top: 30px;
padding-bottom: 20px;
font-size: 18px;"> House #25, Road #4, Block #F <br><br> Banani Dhaka, <br><br> Dhaka 1213 </p>


<!--<p style="text-align: center;-->
<!--font-family:  Poppins;-->
<!--letter-spacing: 0;-->
<!--color: #FFFFFF;-->
<!--opacity: 1;-->

<!--border-bottom: 1px solid gold;-->
<!--display: inline-block;-->
<!--padding-top: 20px;-->
<!--padding-bottom: 5px;-->
<!--padding-right: 50px;-->
<!--padding-left: 50px;"> United States  </p>-->


<!--<p style="text-align: center;-->
<!--font-family:  Roboto;-->
<!--letter-spacing: 0;-->
<!--color: #FFFFFF;-->
<!--opacity: 1;-->
<!--padding-top: 30px;-->
<!--padding-bottom: 50px;-->
<!--font-size: 18px;"> 50W 34th St , Floor 25 ,<br><br> New York ,<br><br> New York, 10001    </p>-->

<img src="images/mt_logo.png" alt="logo" style="width: 60%;" align="center" /> <br>
<img src="images/mt_text.png" alt="logo" style="width: 60%; padding-bottom: 50px;" align="center" />
</section>