<DOCTYPE html>
<html>
<head>
    <!--Redirect to different pages based on Screen Size-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
$(document).ready(function(){
  if($(window).width() >= 1000) {
window.location = "http://www.medinatech.co/ReachUs_MT.php";
}
  if($(window).width() > 750 && $(window).width() < 1000 ) {
window.location = "http://www.medinatech.co/tab/ReachUs_MT.php";
}
});
</script>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Medina Tech || Reach Us</title>

	<link href="https://fonts.googleapis.com/css?family=Poppins:600,700|Roboto&display=swap" rel="stylesheet"> 
    <!--CSS-->
	<link rel="stylesheet" href="../style_MT.css">
	<!--SCRIPTS-->
  <script src="../functions_MT.js"></script>
  
</head>
<body>
<!--Useful links for favicon-->
<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
        <link rel="apple-touch-icon" sizes="57x57" href="../images/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="../images/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="../images/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="../images/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="../images/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="../images/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="../images/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="../images/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="../images/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192" href="../images/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="../images/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="../images/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="../images/favicon-16x16.png">
<!-- --------------------tab menu------------------------- -->

  <div class="tabMenu">

    <div>
      <a href="index.php"><img class="tabMenu_logo" src="../skins/MedinaTech PNG.png" alt="logo"></a>
    </div>

    <ul>

        <li><a  href="index.php"><b>Home Page</b></a></li>
        <li><a href="teams.php"><b>Culture & Career Development</b></a></li>
        <li><a  class="active" href="ReachUs_MT.php"><b>Reach Us</b></a></li>

      </ul>

  </div>

<!-- ---------------mobile menu---------------------------------- -->

  <div class="mobileMenu">

    <a href="index.php"><img style="width: 140px; position: absolute;padding: 2px 10px;" src="../skins/MedinaTech PNG.png" alt="logo"></a>

    <div class="dropdown">
      <img onclick="myFunction()" class="dropbtn" style="width: 25px" src="../skins/menu.png" alt="menu">
      
      <div id="myDropdown" class="dropdown-content">
        <a  href="index.php">Home</a>
        <a href="teams.php">Culture & Career Development</a>
        <a class="active" href="ReachUs_MT.php">Reach Us</a>
      </div>
    </div>
  </div>

  <!-- ---------------pc navbar---------------------------------- -->

  <div class="navbar">
      
      <ul>

        <li><a class="active" href="ReachUs_MT.php"><b>Reach Us</b></a></li>
        <li><a href="teams.php"><b>Culture & Career Development</b></a></li>
        <li><a href="index.php"><b>Home Page</b></a></li>

      </ul>
      <a href="index.php"><img class="lionLogo" src="../skins/lion.png" alt="logo"></a>
    
  </div>

  <!-- ------------------------------------ -->
  <div class="social">
    <a href="http://www.facebook.com/sharer/sharer.php?u=https://medinatech.co" target="_blank"><img src="../skins/fb.png" alt="logo"></a>
    
    <a href="https://www.linkedin.com/shareArticle?mini=true&url=https%3A%2F%2Fwww.medinatech.co&title=Medina+Tech" target="_blank"><img src="../skins/in.png" alt="logo">  </a>
  </div>

<!--Reach Us Section -->


<div class="gridFull" style="width: 100%;">
<div class="allItem" style="width: 90%;">


    <div class="form_" style="margin: 0px;margin-left: ;
margin-top: 30px;">
      <p class="dropline" style="text-align: left;
font: SemiBold 40px/60px Poppins;
letter-spacing: 0;
color: #000000;
text-transform: uppercase;
opacity: 1;
font-weight: bold;
padding-top: 10px;">Reach Us</p>

      <div>
          <!--FORM-->
        <form action="/action_page.php" style="width:90%;">

          <input class="formHalfBox" type="text" id="fname" name="firstname" placeholder="Full Name">

          <input class="formHalfBox" type="text" id="lname" name="lastname" placeholder="E-mail"> <br>

          <input class="formFullBox" type="text" id="lname" name="lastname" placeholder="Subject">

          <input class="formFullBox" type="text" id="lname" name="lastname" placeholder="Message"><br>

          
        
          <input type="submit" value="Send Message">
        </form>
      </div>
      
    </div>

  </div>

</div> <!-- allItem -->
</div> <!-- gridFull -->
<!--Adding Footer Section -->
 <?php
 include "footer_new.php";
 ?>

</body>
</html>

