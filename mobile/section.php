<section>
                <p class="heading" style="margin-top: 0% ; font-size: 30px; padding-bottom: 20px; padding-top: 20px; ">Our Team <br> </p>
                <!-- Medina Ma'am -->
                <div class="rowt">
                    
                    <div class="columnt" style="width:70%" align="left">
                        <div class="teamt">
                            <div class="team_title" style="padding-left: 45px;height: 50px;margin-top: 50px;

width: 100%;

background: #1D1D1D;

color: white;

text-align: left;

font: Bold 15px/30px Poppins;

letter-spacing: 0;



opacity: 1;

margin-top: 60px;">

                                <div class="rowt">
                                    <div class="columnt" style="flex: 50%;

padding: 5px;

margin-left: -10px;" align="right">
                                        <!--<p style="padding-top: 0px; align-content: left; padding-left: 15px;margin-top: -8px;"> Medina Ali</p>-->

                                        <p style="margin-top: 5px; color:#D4AF37; margin-bottom :10px"><font style="font: Bold 15px/20px Poppins; align-content: left; padding-left: 15px;" color="#D4AF37">CEO</font> </p>
                                        <p style="margin-top: 17px;color: black ; font:Italic 15px/20px Poppins; /*! padding-top: 15px */align-content: left;text-align: right;font-size: 10px;margin-left: -40px;font-weight: bold;" align="left">
                                            “if you are not willing to sit with uncertainty, you will kill possibility”<br><small>- TEDx Talks Keren Eldad</small>
                                        </p>
                                        
                                       
                                        <!--<a href="https://www.linkedin.com/in/medinaali/" target="_blank"><img src="../images/linkedin.png" alt="logo" style="padding-left: 15px;height: 20px;width: 20px;" align="left"></a>-->
                                        <!--<a href="https://twitter.com/medinaali_" target="_blank"><img src="../images/twitter-sign.png" alt="logo" style="padding-left: 15px;height: 20px;width: 20px;" align="left"></a>-->
                                    </div>
                                    <div class="columnt" style="width: 100%;

margin-top: -20px;" align="left">
                                        <a href="#apply">
                                            <img src="../images/team/medina-mam.png" alt="CEO" style="width:100%" align="left">
                                        </a>
                                    </div>
                                </div>
                                <!--      
               <div class="team_content" >
           Content
        </div>-->

                            </div>
                        </div>
                    </div>
                </div>
                 <!-- HR -->

                <div class="rowt">
                    
                    <div class="columnt" style="width:70%;margin-top: -120px;" align="left">
                        <div class="teamt">
                            <div class="team_title" style="padding-left: 45px;height: 50px;margin-top: 50px;

width: 100%;

background: #1D1D1D;

color: white;

text-align: left;

font: Bold 15px/30px Poppins;

letter-spacing: 0;


opacity: 1;

margin-top: 60px;">

                                <div class="rowt">
                                    <div class="columnt" style="width: 100%;

margin-top: 0px; margin-left: -41px;" align="left">
                                        <a href="#apply">
                                            <img src="../images/avatar/ben.png" alt="HR" style="width:65%" align="left">
                                        </a>
                                    </div>
                                    <div class="columnt" style="flex: 50%;

padding: 5px;

margin-left: -10px;" align="left">
                                        <!--<p style="padding-top: 0px; align-content: left; padding-left: 15px;margin-top: -8px;margin-left: -30px;"> Ariba Alam</p>-->

                                        <p style="margin-top: 5px; color:#D4AF37; margin-bottom :10px"><font style="font: Bold 13px/20px Poppins; align-content: left; padding-left: 15px;margin-left: -30px;" color="#D4AF37">HR & Admin</font> </p>
                                        <p style="margin-top: 17px;color: black ; font:Italic 15px/20px Poppins; /*! padding-top: 15px */align-content: left;text-align: left;font-size: 10px;margin-left: -10px;font-weight: bold;" align="left">
                                           " In the practice of tolerance, one’s enemy is the best teacher. "<br><small>- Dalai Lama</small>
                                        </p>
                                        <br>
                                       
                                        <!--<a href="https://www.linkedin.com/in/syeda-ariba-alam-67a072199/" target="_blank"><img src="../images/linkedin.png" alt="logo" style="padding-left: 15px;height: 20px;width: 20px;" align="right"></a>-->
                                        
                                    </div>
                                    
                                </div>
                                <!--      
               <div class="team_content" >
           Content
        </div>-->

                            </div>
                        </div>
                    </div>
                </div>
                <!-- Ariba -->

                <div class="rowt">
                    
                    <div class="columnt" style="width:70%;margin-top: -120px;" align="left">
                        <div class="teamt">
                            <div class="team_title" style="padding-left: 45px;height: 50px;margin-top: 50px;

width: 100%;

background: #1D1D1D;

color: white;

text-align: left;

font: Bold 15px/30px Poppins;

letter-spacing: 0;


opacity: 1;

margin-top: 60px;">

                                <div class="rowt">
                                    <div class="columnt" style="width: 100%;

margin-top: -20px; margin-left: -60px;" align="left">
                                        <a href="#apply">
                                            <img src="../images/avatar/ariba.png" alt="AI& MACHINE LEARNING" style="width:100%" align="left">
                                        </a>
                                    </div>
                                    <div class="columnt" style="flex: 50%;

padding: 5px;

margin-left: -10px;" align="left">
                                        <!--<p style="padding-top: 0px; align-content: left; padding-left: 15px;margin-top: -8px;margin-left: -30px;"> Ariba Alam</p>-->

                                        <p style="margin-top: 5px; color:#D4AF37; margin-bottom :10px"><font style="font: Bold 13px/20px Poppins; align-content: left; padding-left: 15px;margin-left: -30px;" color="#D4AF37">Ai &amp; ML Specialist</font> </p>
                                        <p style="margin-top: 17px;color: black ; font:Italic 15px/20px Poppins; /*! padding-top: 15px */align-content: left;text-align: left;font-size: 10px;margin-left: -10px;font-weight: bold;" align="left">
                                           " You can't go back and change the beginning, but you can start where you are and change the ending "<br><small>- C.S.Lewis</small>
                                        </p>
                                        <br>
                                       
                                        <!--<a href="https://www.linkedin.com/in/syeda-ariba-alam-67a072199/" target="_blank"><img src="../images/linkedin.png" alt="logo" style="padding-left: 15px;height: 20px;width: 20px;" align="right"></a>-->
                                        
                                    </div>
                                    
                                </div>
                                <!--      
               <div class="team_content" >
           Content
        </div>-->

                            </div>
                        </div>
                    </div>
                </div>

                <!-- Mustakim -->
                 <div class="rowt">
                    
                    <div class="columnt" style="width:70%;margin-top: -120px;" align="left">
                        <div class="teamt">
                            <div class="team_title" style="padding-left: 45px;height: 50px;margin-top: 50px;

width: 100%;

background: #1D1D1D;

color: white;

text-align: left;

font: Bold 15px/30px Poppins;

letter-spacing: 0;


opacity: 1;

margin-top: 60px;">

                                <div class="rowt">
                                    <div class="columnt" style="width: 100%;

margin-top: -20px; margin-left: -60px;" align="left">
                                        <a href="#apply">
                                            <img src="../images/avatar/mustakim.png" alt="Backend & DevOps" style="width:100%" align="left">
                                        </a>
                                    </div>
                                    <div class="columnt" style="flex: 50%;

padding: 5px;

margin-left: -10px;" align="left">
                                        <!--<p style="padding-top: 0px; align-content: left; padding-left: 15px;margin-top: -8px;margin-left: -30px;"> Mustakim Hussain</p>-->

                                        <p style="margin-top: 5px; color:#D4AF37; margin-bottom :10px"><font style="font: Bold 13px/20px Poppins; align-content: left; padding-left: 15px;margin-left: -30px;" color="#D4AF37">Back-End &amp; DevOps</font> </p>
                                        <p style="margin-top: 17px;color: black ; font:Italic 15px/20px Poppins; /*! padding-top: 15px */align-content: left;text-align: left;font-size: 10px;margin-left: -10px;font-weight: bold;" align="left">
                                           " Be a yardstick of quality. Some people aren't used to an environment where excellence is expected"<br><small>- Elon Musk</small>
                                        </p>
                                        <br>
                                       
                                        <!--<a href="https://www.linkedin.com/in/mustakim-hussain-657022ba/" target="_blank"><img src="../images/linkedin.png" alt="logo" style="padding-left: 15px;height: 20px;width: 20px;" align="right"></a>-->
                                        
                                    </div>
                                    
                                </div>
                                <!--      
               <div class="team_content" >
           Content
        </div>-->

                            </div>
                        </div>
                    </div>
                </div>
                 <!-- Nibras -->
                 <div class="rowt">
                    
                    <div class="columnt" style="width:70%;margin-top: -120px;" align="left">
                        <div class="teamt">
                            <div class="team_title" style="padding-left: 45px;height: 50px;margin-top: 50px;

width: 100%;

background: #1D1D1D;

color: white;

text-align: left;

font: Bold 15px/30px Poppins;

letter-spacing: 0;


opacity: 1;

margin-top: 60px;">

                                <div class="rowt">
                                    <div class="columnt" style="width: 100%;

margin-top: -20px; margin-left: -60px;" align="left">
                                        <a href="#apply">
                                            <img src="../images/avatar/nibras.png" alt="UI/UX" style="width:100%;margin-left: -20px;" align="left">
                                        </a>
                                    </div>
                                    <div class="columnt" style="flex: 50%;

padding: 5px;

margin-left: -10px;" align="left">
                                        <!--<p style="padding-top: 0px; align-content: left; padding-left: 15px;margin-top: -8px;margin-left: -30px;"> Nibras Khan</p>-->

                                        <p style="margin-top: 5px; color:#D4AF37; margin-bottom :10px"><font style="font: Bold 13px/20px Poppins; align-content: left; padding-left: 15px;margin-left: -30px;" color="#D4AF37">UI/UX Developer</font> </p>
                                        <p style="margin-top: 17px;color: black ; font:Italic 15px/20px Poppins; /*! padding-top: 15px */align-content: left;text-align: left;font-size: 10px;margin-left: -10px;font-weight: bold;" align="left">
                                           "  If work isn't fun,<br> you're not playing on th right team "<br><small>- Frank Sonenbarg</small>
                                        </p>
                                        <br>
                                       
                                        <!--<a href="https://www.linkedin.com/in/nibraskhan35/" target="_blank"><img src="../images/linkedin.png" alt="logo" style="padding-left: 15px;height: 20px;width: 20px;" align="right"></a>-->
                                        
                                    </div>
                                    
                                </div>
                                <!--      
               <div class="team_content" >
           Content
        </div>-->

                            </div>
                        </div>
                    </div>
                </div>


                 <!-- Kowshik -->
                 <div class="rowt">
                    
                    <div class="columnt" style="width:70%;margin-top: -120px;" align="left">
                        <div class="teamt">
                            <div class="team_title" style="padding-left: 45px;height: 50px;margin-top: 50px;

width: 100%;

background: #1D1D1D;

color: white;

text-align: left;

font: Bold 15px/30px Poppins;

letter-spacing: 0;


opacity: 1;

margin-top: 60px;">

                                <div class="rowt">
                                    <div class="columnt" style="width: 100%;

margin-top: -20px; margin-left: -60px;" align="left">
                                        <a href="#apply">
                                            <img src="../images/avatar/kowshik.png" alt="Ai & Machine Learning" style="width:100%;margin-left: -10px;" align="left">
                                        </a>
                                    </div>
                                    <div class="columnt" style="flex: 50%;

padding: 5px;

margin-left: -10px;" align="left">
                                        <!--<p style="padding-top: 0px; align-content: left; padding-left: 15px;margin-top: -8px;margin-left: -30px;"> Kowshik Mazumder</p>-->

                                        <p style="margin-top: 5px; color:#D4AF37; margin-bottom :10px"><font style="font: Bold 13px/20px Poppins; align-content: left; padding-left: 15px;margin-left: -30px;" color="#D4AF37">Ai &amp; ML Specialist</font> </p>
                                        <p style="margin-top: 17px;color: black ; font:Italic 15px/20px Poppins; /*! padding-top: 15px */align-content: left;text-align: left;font-size: 10px;margin-left: -10px;font-weight: bold;" align="left">
                                           "  Insanity : doing the same thing over and over again and expecting different results."<br><small>- Albert Einstein</small>
                                        </p>
                                        <br>
                                       
                                        <!--<a href="https://www.linkedin.com/in/kowshik-majumder-3687b6133/" target="_blank"><img src="../images/linkedin.png" alt="logo" style="padding-left: 15px;height: 20px;width: 20px;" align="right"></a>-->
                                        
                                    </div>
                                    
                                </div>
                                <!--      
               <div class="team_content" >
           Content
        </div>-->

                            </div>
                        </div>
                    </div>
                </div>

                <!-- Abu Bakkar -->
                 <div class="rowt">
                    
                    <div class="columnt" style="width:70%;margin-top: -120px;" align="left">
                        <div class="teamt">
                            <div class="team_title" style="padding-left: 45px;height: 50px;margin-top: 50px;

width: 100%;

background: #1D1D1D;

color: white;

text-align: left;

font: Bold 15px/30px Poppins;

letter-spacing: 0;
opacity: 1;

margin-top: 60px;">

                                <div class="rowt">
                                    <div class="columnt" style="width: 100%;

margin-top: -20px; margin-left: -60px;" align="left">
                                        <a href="#apply">
                                            <img src="../images/avatar/shuvo.png" alt="Research Specialist" style="width:100%;margin-left: -10px;" align="left">
                                        </a>
                                    </div>
                                    <div class="columnt" style="flex: 50%;

padding: 5px;

margin-left: -10px;" align="left">
                                        <!--<p style="padding-top: 0px; align-content: left; padding-left: 15px;margin-top: -8px;margin-left: -30px;font-size: 14px;"> Abu Bakkar Shiddique</p>-->

                                        <p style="margin-top: 5px; color:#D4AF37; margin-bottom :10px"><font style="font: Bold 13px/20px Poppins; align-content: left; padding-left: 15px;margin-left: -30px;" color="#D4AF37">Research Specialist</font> </p>
                                        <p style="margin-top: 17px;color: black ; font:Italic 15px/20px Poppins; /*! padding-top: 15px */align-content: left;text-align: left;font-size: 10px;margin-left: -10px;font-weight: bold;" align="left">
                                           "  The meaning of life is to fall seven times and get up at eighth"<br><small>- Paulo Coelho</small>
                                        </p>
                                        <br>
                                       
                                        <!--<a href="https://www.linkedin.com/in/md-abubakkar-shiddique-shovo/" target="_blank"><img src="../images/linkedin.png" alt="logo" style="padding-left: 15px;height: 20px;width: 20px;" align="right"></a>-->
                                        
                                    </div>
                                    
                                </div>
                                <!--      
               <div class="team_content" >
           Content
        </div>-->

                            </div>
                        </div>
                    </div>
                </div>

            <!-- JAFRY -->
                 <div class="rowt">
                    
                    <div class="columnt" style="width:70%;margin-top: -120px;" align="left">
                        <div class="teamt">
                            <div class="team_title" style="padding-left: 45px;height: 50px;margin-top: 50px;

width: 100%;

background: #1D1D1D;

color: white;

text-align: left;

font: Bold 15px/30px Poppins;

letter-spacing: 0;

opacity: 1;

margin-top: 60px;">

                                <div class="rowt">
                                    <div class="columnt" style="width: 100%;

margin-top: -20px; margin-left: -60px;" align="left">
                                        <a href="#apply">
                                            <img src="../images/avatar/jafry.png" alt="Front End Developer" style="width:100%;margin-left: -10px;" align="left">
                                        </a>
                                    </div>
                                    <div class="columnt" style="flex: 50%;

padding: 5px;

margin-left: -10px;" align="left">
                                        <!--<p style="padding-top: 0px; align-content: left; padding-left: 15px;margin-top: -8px;margin-left: -30px;font-size: 14px;"> Jafry Deep</p>-->

                                        <p style="margin-top: 5px; color:#D4AF37; margin-bottom :10px"><font style="font: Bold 13px/20px Poppins; align-content: left; padding-left: 15px;margin-left: -30px;" color="#D4AF37">Front-End Developer</font> </p>
                                        <p style="margin-top: 17px;color: black ; font:Italic 15px/20px Poppins; /*! padding-top: 15px */align-content: left;text-align: left;font-size: 10px;margin-left: -10px;font-weight: bold;" align="left">
                                           " A winner is a Dreamer who Never gives up."<br><small>- Nelson Mandela</small>
                                        </p>
                                        <br>
                                       
                                        <!--<a href="https://www.linkedin.com/in/jafry-deep-a926b6160/" target="_blank"><img src="../images/linkedin.png" alt="logo" style="padding-left: 15px;height: 20px;width: 20px;" align="right"></a>-->
                                        
                                    </div>
                                    
                                </div>
                                <!--      
               <div class="team_content" >
           Content
        </div>-->

                            </div>
                        </div>
                    </div>
                </div>
            <!--Ashraful-->
            <div class="rowt">
                    
                    <div class="columnt" style="width:70%;margin-top: -120px;" align="left">
                        <div class="teamt">
                            <div class="team_title" style="padding-left: 45px;height: 50px;margin-top: 50px;

width: 100%;

background: #1D1D1D;

color: white;

text-align: left;

font: Bold 15px/30px Poppins;

letter-spacing: 0;
opacity: 1;

margin-top: 60px;">

                                <div class="rowt">
                                    <div class="columnt" style="width: 100%;

margin-top: -20px; margin-left: -60px;" align="left">
                                        <a href="#apply">
                                            <img src="../images/team/ashraful.png" alt="Full Stack" style="width:100%;margin-left: -10px;" align="left">
                                        </a>
                                    </div>
                                    <div class="columnt" style="flex: 50%;

padding: 5px;

margin-left: -10px;" align="left">
                                        <!--<p style="padding-top: 0px; align-content: left; padding-left: 15px;margin-top: -8px;margin-left: -30px;font-size: 14px;"> Ashraful Islam Sheiblu</p>-->

                                        <p style="margin-top: 5px; color:#D4AF37; margin-bottom :10px"><font style="font: Bold 13px/20px Poppins; align-content: left; padding-left: 15px;margin-left: -30px;" color="#D4AF37">Full Stack Developer</font> </p>
                                        <p style="margin-top: 17px;color: black ; font:Italic 15px/20px Poppins; /*! padding-top: 15px */align-content: left;text-align: left;font-size: 10px;margin-left: -10px;font-weight: bold;" align="left">
                                           "  The meaning of life is to fall seven times and get up at eighth"<br><small>- Theodore Roosevelt</small>
                                        </p>
                                        <br>
                                       
                                        <!--<a href="https://www.linkedin.com/in/ashraful-islam-sheiblu-4a96a0b9/" target="_blank"><img src="../images/linkedin.png" alt="logo" style="padding-left: 15px;height: 20px;width: 20px;" align="right"></a>-->
                                        
                                    </div>
                                    
                                </div>
                                <!--      
               <div class="team_content" >
           Content
        </div>-->

                            </div>
                        </div>
                    </div>
                </div>
                
            <div class="rowt">
                    
                    <div class="columnt" style="width:70%;margin-top: -120px;" align="left">
                        <div class="teamt">
                            <div class="team_title" style="padding-left: 45px;height: 50px;margin-top: 50px;

width: 100%;

background: #1D1D1D;

color: white;

text-align: left;

font: Bold 15px/30px Poppins;

letter-spacing: 0;
opacity: 1;

margin-top: 60px;">

                                <div class="rowt">
                                    <div class="columnt" style="width: 100%;

margin-top: -20px; margin-left: -60px;" align="left">
                                        <a href="#apply">
                                            <img src="../images/team/shovon.png" alt="Full Stack" style="width:100%;margin-left: -10px;" align="left">
                                        </a>
                                    </div>
                                    <div class="columnt" style="flex: 50%;

padding: 5px;

margin-left: -10px;" align="left">
                                        <!--<p style="padding-top: 0px; align-content: left; padding-left: 15px;margin-top: -8px;margin-left: -30px;font-size: 12px;"> Shovon Lal Chakraborty</p>-->

                                        <p style="margin-top: 5px; color:#D4AF37; margin-bottom :10px"><font style="font: Bold 13px/20px Poppins; align-content: left; padding-left: 15px;margin-left: -30px;" color="#D4AF37">Full Stack Developer</font> </p>
                                        <p style="margin-top: 17px;color: black ; font:Italic 15px/20px Poppins; /*! padding-top: 15px */align-content: left;text-align: left;font-size: 10px;margin-left: -10px;font-weight: bold;" align="left">
                                           " Know Thyself " <br><br><small>- Socrates</small>
                                        </p>
                                        <br>
                                       
                                        <!--<a href="#" target="_blank"><img src="../images/linkedin.png" alt="logo" style="padding-left: 15px;height: 20px;width: 20px;" align="right"></a>-->
                                        
                                    </div>
                                    
                                </div>
                                <!--      
               <div class="team_content" >
           Content
        </div>-->

                            </div>
                        </div>
                    </div>
                </div>   
                
        <div class="rowt" style="margin-top: -30px;" >
                    
                    <div class="columnt" style="width:70%;margin-top: -120px;" align="left">
                        <div class="teamt">
                            <div class="team_title" style="padding-left: 45px;height: 50px;margin-top: 50px;

width: 100%;

background: #1D1D1D;

color: white;

text-align: left;

font: Bold 15px/30px Poppins;

letter-spacing: 0;
opacity: 1;

margin-top: 60px;">

                                <div class="rowt">
                                    <div class="columnt" style="width: 100%;

margin-top: -20px; margin-left: -60px;" align="left">
                                        <a href="#apply">
                                            <img src="../images/team/shahnewaz.png" alt="Full Stack" style="width:100%;margin-left: -10px;" align="left">
                                        </a>
                                    </div>
                                    <div class="columnt" style="flex: 50%;

padding: 5px;

margin-left: -10px;" align="left">
                                        <!--<p style="padding-top: 0px; align-content: left; padding-left: 15px;margin-top: -8px;margin-left: -30px;font-size: 12px;">Md. Shahnawaz Hossain</p>-->

                                        <p style="margin-top: 5px; color:#D4AF37; margin-bottom :10px"><font style="font: Bold 13px/20px Poppins; align-content: left; padding-left: 15px;margin-left: -30px;" color="#D4AF37">AI / ML Intern </font> </p>
                                        <p style="margin-top: 17px;color: black ; font:Italic 15px/20px Poppins; /*! padding-top: 15px */align-content: left;text-align: left;font-size: 10px;margin-left: -10px;font-weight: bold;" align="left">
                                           " To be yourself in the world, where things nearby trying to make you involved in anything else is the best accomplishment, I guess, being a user-defined homo sapiens. " <br><small>- Yuval Harari</small>
                                        </p>
                                        <br>
                                       
                                        <!--<a href="https://www.linkedin.com/in/md-shahnawaz-hossain-827b2019b/" target="_blank"><img src="../images/linkedin.png" alt="logo" style="padding-left: 15px;height: 20px;width: 20px;" align="right"></a>-->
                                        
                                    </div>
                                    
                                </div>
                                <!--      
               <div class="team_content" >
           Content
        </div>-->

                            </div>
                        </div>
                    </div>
                </div>
                
            <div class="rowt"  style="margin-top: 50px;">
                    
                    <div class="columnt" style="width:70%;margin-top: -120px;" align="left">
                        <div class="teamt">
                            <div class="team_title" style="padding-left: 45px;height: 50px;margin-top: 50px;

width: 100%;

background: #1D1D1D;

color: white;

text-align: left;

font: Bold 15px/30px Poppins;

letter-spacing: 0;
opacity: 1;

margin-top: 60px;">

                                <div class="rowt">
                                    <div class="columnt" style="width: 100%;

margin-top: -20px; margin-left: -60px;" align="left">
                                        <a href="#apply">
                                            <img src="../images/team/tan.png" alt="Full Stack" style="width:100%;margin-left: -10px;" align="left">
                                        </a>
                                    </div>
                                    <div class="columnt" style="flex: 50%;

padding: 5px;

margin-left: -10px;" align="left">
                                        <!--<p style="padding-top: 0px; align-content: left; padding-left: 15px;margin-top: -8px;margin-left: -30px;font-size: 12px;">Md. Shahnawaz Hossain</p>-->

                                        <p style="margin-top: -5px; color:#D4AF37; margin-bottom :10px"><font style="font: Bold 13px/20px Poppins; align-content: left; padding-left: 15px;margin-left: -30px;" color="#D4AF37">Financial planning & </font> </p>
                                        <p style="margin-top: -15px; color:#D4AF37; margin-bottom :10px"><font style="font: Bold 13px/20px Poppins; align-content: left; padding-left: 15px;margin-left: -30px;" color="#D4AF37">Analysis Manager </font> </p>
                                        <p style="margin-top: 17px;color: black ; font:Italic 15px/20px Poppins; /*! padding-top: 15px */align-content: left;text-align: left;font-size: 10px;margin-left: -10px;font-weight: bold;" align="left">
                                           " The way to get started is to quit talking and begin doing "  <br><small> - Walt Disney</small>
                                        </p>
                                        <br>
                                       
                                        <!--<a href="https://www.linkedin.com/in/md-shahnawaz-hossain-827b2019b/" target="_blank"><img src="../images/linkedin.png" alt="logo" style="padding-left: 15px;height: 20px;width: 20px;" align="right"></a>-->
                                        
                                    </div>
                                    
                                </div>
                                <!--      
               <div class="team_content" >
           Content
        </div>-->

                            </div>
                        </div>
                    </div>
                </div>
                
            </section>