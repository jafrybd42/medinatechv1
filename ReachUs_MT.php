<DOCTYPE html>
<html>
<head>
    <!--Redirect to defferent pages based on screen width-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
$(document).ready(function(){
  if($(window).width() <= 750) {
window.location = "/mobile/ReachUs_MT.php";
}
  if($(window).width() > 750 && $(window).width() < 1000 ) {
window.location = "/tab/ReachUs_MT.php";
}
});
</script>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Medina Tech || Reach Us</title>
    <!--Fonts-->
	<link href="https://fonts.googleapis.com/css?family=Poppins:600,700|Roboto&display=swap" rel="stylesheet"> 
    <!--CSS-->
	<link rel="stylesheet" href="style_MT.css">
	<!--Scripts-->
  <script src="functions_MT.js"></script>
  
</head>
<body>
    <!--Icon size-->
  <link rel="apple-touch-icon" sizes="57x57" href="images/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="images/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="images/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="images/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="images/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="images/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="images/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="images/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="images/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="images/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="images/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="images/favicon-16x16.png">
<!-- --------------------tab menu------------------------- -->

  <div class="tabMenu">

    <div>
      <a href="index.php"><img class="tabMenu_logo" src="skins/MedinaTech PNG.png" alt="logo"></a>
    </div>

    <ul>

        <li><a  href="index.php"><b>Home Page</b></a></li>
        <li><a href="teams.php"><b>Culture & Career Development</b></a></li>
        <li><a  class="active" href="ReachUs_MT.php"><b>Reach Us</b></a></li>

      </ul>

  </div>

<!-- ---------------mobile menu---------------------------------- -->

  <div class="mobileMenu">

    <a href="index.php"><img style="width: 140px; position: absolute;padding: 2px 10px;" src="skins/MedinaTech PNG.png" alt="logo"></a>

    <div class="dropdown">
      <img onclick="myFunction()" class="dropbtn" style="width: 25px" src="skins/menu.png" alt="menu">
      
      <div id="myDropdown" class="dropdown-content">
        <a  href="index.php">Home</a>
        <a href="teams.php">Culture & Career Development</a>
        <a class="active" href="ReachUs_MT.php">Reach Us</a>
      </div>
    </div>
  </div>

  <!-- ---------------pc navbar---------------------------------- -->

  <div class="navbar">
      
      <ul>

        <li><a class="active" href="ReachUs_MT.php"><b>Reach Us</b></a></li>
        <li><a href="teams.php"><b>Culture & Career Development</b></a></li>
        <li><a href="index.php"><b>Home Page</b></a></li>

      </ul>
      <a href="index.php"><img class="lionLogo" src="skins/lion.png" alt="logo"></a>
    
  </div>

  <!-- ------------------------------------ -->
  <!--<div class="social">-->
  <!--  <a href="http://www.facebook.com/sharer/sharer.php?u=https://medinatech.co" target="_blank"><img src="skins/fb.png"></a>-->
    
  <!--  <a href="https://www.linkedin.com/shareArticle?mini=true&url=https%3A%2F%2Fwww.medinatech.co&title=Medina+Tech" target="_blank"><img src="skins/in.png">  </a>-->
  <!--</div>-->

  <!-- -----------Reach Us Section----------------- -->


<div class="gridFull" >
<div class="allItem">

  <div class="gridReachUs" style="height:94%">
    <div class="reachUs" style="background-size: 100%;">

      <div class="reachUsContants">
          <!--//edited-->
<!--      <p style="position: relative;-->
<!--margin-left: -100px;-->
<!--text-align: center;-->
<!--width: 250px;-->
<!--height: 150px;-->
<!--margin-top: 10px;font-size:50px;" class="heading">REACH US</p>-->
<!--//edited-->

      <!-- <img class="reachUsSocial" src="skins/Group 18.png">
      <img class="reachUsSocial" src="skins/Group 19.png"><br>
      <img class="reachUsSocial" src="skins/Group 20.png"> -->
      </div>
      
    </div>

    <div class="form_">
      <p class="dropline" style="font-size:25px;">DROP A LINE</p>

      <div>
        <form action="reach_us_config.php" method="post">

          <input class="formHalfBox" type="text" id="name" name="name" placeholder="Full Name">

          <input class="formHalfBox" type="text" id="email" name="email" placeholder="E-mail"> <br>

          <input class="formFullBox" type="text" id="subject" name="subject" placeholder="Subject">

          <input class="formFullBox" type="text" id="message" name="message" placeholder="Message"><br>

          
        
          <input type="submit" value="Send Message">
        </form>
      </div>
      
    </div>

  </div>




<!--Footer Section-->

 <?php
 include "footer_new.php";
 ?>






</div> <!-- allItem -->
</div> <!-- gridFull -->
</body>
</html>

