<!-- <html>
<head>
  <script> window.location.href = 'desktop/index.php'; </script>
</head>
</html> -->


<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<title>Medina Tech</title>

<!--CSS-->
<link rel="stylesheet" href="counterFIles/css/style.css">
<link rel="stylesheet" href="counterFIles/css/bootstrap-light.css">
<link href='https://fonts.googleapis.com/css?family=Oswald:300|Poppins:400,700' rel='stylesheet' type='text/css'>
<!--/CSS-->

<!--JS-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="counterFIles/js/jquery.plugin.js"></script>
<script src="counterFIles/js/jquery.countdown.js"></script>
<script>
$(function () {
	$('#defaultCountdown').countdown({until: new Date(2021,6,10)});
});
</script>
<!--/JS-->

</head>

<body>

<!--DARK OVERLAY-->
<div class="overlay"></div>
<!--/DARK OVERLAY-->

<!--WRAP-->
<div id="wrap">
	<!--CONTAINER-->
	<div class="container">
		<div class="envelope">
			<img src="counterFIles/images/mtt.png" width="150px" />
		</div>
		<h1>
			<span class="small">We Are Tunning</span>
			<span class="big">Medina Tech Website<span class="yellow">.</span></span>
		</h1>
		<p>Our site is currently under construction but we are working hard to bring you the new design.</p>
		<div id="defaultCountdown"></div>
	</div>
	<!--/CONTAINER-->
</div>
<!--/WRAP-->

</body>
</html>
