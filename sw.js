const staticCacheName = 'MTcache-v1';

const assets = [
    './',
    './index.php',
    './teams.php',
    './ReachUs_MT.php',
    './mobile/index.php',
    './mobile/teams.php',
    './mobile/ReachUs_MT.php',
    './team/welcome.php',
    './team/profile.php',
    './team/edit_profile.php',
    './team/index.php',
    './team/files.php',

    '.skins/__black__font.png',
    '.skins/a_01.png',
    '.skins/a-01@2x.png',
    '.skins/class.png',
    '.skins/download.png',
    '.skins/education.gif',
    '.skins/fb - Copy.png',
    '.skins/fb.png',
    '.skins/Finance.gif',
    '.skins/font.png',
    '.skins/footerlion.png',
    '.skins/FULL(mobile).gif',
    '.skins/FULL.gif',
    '.skins/grey_ek1.png',
    '.skins/Group 5.png',
    '.skins/Group 13.png',
    '.skins/Group 15.png',
    '.skins/group_5.png',
    '.skins/icon_awesome_facebook_square.png',
    '.skins/icon_awesome_facebook_square_ek1.png',
    '.skins/icon_awesome_instagram',
    '.skins/icon_awesome_instagram_ek1.png',
    '.skins/icon_awesome_linkedin.png',
    '.skins/icon_awesome_linkedin_ek1.png',
    '.skins/ig - Copy.png',
    '.skins/ig.png',
    '.skins/in - Copy.png',
    '.skins/in.png',
    '.skins/Infrastructure.gif',
    '.skins/lion - Copy.png',
    '.skins/lion.png',
    '.skins/lion_ek1.png',
    '.skins/management_icon_teamwork_vector_260nw_1412354495.png',
    '.skins/MedinaTech PNG.png',
    '.skins/menu.png',
    '.skins/presentation.png'
];



// install event
self.addEventListener('install', evt => {
  //console.log('service worker installed');
  evt.waitUntil(
    caches.open(staticCacheName).then((cache) => {
      console.log('caching shell assets');
      cache.addAll(assets);
    })
  );
});

// activate event
self.addEventListener('activate', evt => {
  //console.log('service worker activated');
  evt.waitUntil(
    caches.keys().then(keys => {
      //console.log(keys);
      return Promise.all(keys
        .filter(key => key !== staticCacheName)
        .map(key => caches.delete(key))
      );
    })
  );
});

// fetch event
self.addEventListener('fetch', evt => {
  //console.log('fetch event', evt);
  evt.respondWith(
    caches.match(evt.request).then(cacheRes => {
      return cacheRes || fetch(evt.request);
    })
  );
});