<?php
session_start();
include('includes/header.php'); 
include('includes/navbar.php'); 

// Check if the user is logged in, if not then redirect him to login page

if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: index.php");
    exit;
}

?>


<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Team Members</h1>
    <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
        class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
  </div>

  <!-- Content Row -->
  <div class="row">

<!-- //profile -->
  	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.10.2/css/all.css">
  	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>


<div class="container">
	<div class="row">
		<div class="col-sm-6 col-md-4">
			<div class="div-box">
				<div class="User-img">
					<img src="/images/user-icon.jpg">
				</div>
				<h3 class="User-name">Rajnish Kumar</h3>
				<h4 class="designation">Web Developer</h4>
				<div class="contact-btn">
					<button type="button" class="btn btn-success">Follow</button>
					<button type="button" class="btn btn-secondary">Message</button>
				</div>
				<div class="profile-details">
					<ul>
						<li><a href="#"><i class="fas fa-home"></i> Overview</a></li>
						<li><a href="#"><i class="fas fa-user"></i> User Info</a></li>
						<li><a href="#"><i class="fas fa-check"></i> Tasks</a></li>
						<li><a href="#"><i class="fas fa-flag"></i> Help</a></li>
				  </ul>
				</div>
			</div>
		</div>

		<div class="col-sm-6 col-md-4">
			<div class="div-box">
				<div class="User-img">
					<img src="/images/user-icon.jpg">
				</div>
				<h3 class="User-name">Pardeep Singh</h3>
				<h4 class="designation">Web Designer</h4>
				<div class="contact-btn">
					<button type="button" class="btn btn-success">Follow</button>
					<button type="button" class="btn btn-secondary">Message</button>
				</div>
				<div class="profile-details">
					<ul>
						<li><a href="#"><i class="fas fa-home"></i> Overview</a></li>
						<li><a href="#"><i class="fas fa-user"></i> User Info</a></li>
						<li><a href="#"><i class="fas fa-check"></i> Tasks</a></li>
						<li><a href="#"><i class="fas fa-flag"></i> Help</a></li>
				  </ul>
				</div>
			</div>
		</div>

		<div class="col-sm-6 col-md-4">
			<div class="div-box">
				<div class="User-img">
					<img src="/images/user-icon.jpg">
				</div>
				<h3 class="User-name">Gaurav Kumar</h3>
				<h4 class="designation">Tester</h4>
				<div class="contact-btn">
					<button type="button" class="btn btn-success">Follow</button>
					<button type="button" class="btn btn-secondary">Message</button>
				</div>
				<div class="profile-details">
					<ul>
						<li><a href="#"><i class="fas fa-home"></i> Overview</a></li>
						<li><a href="#"><i class="fas fa-user"></i> User Info</a></li>
						<li><a href="#"><i class="fas fa-check"></i> Tasks</a></li>
						<li><a href="#"><i class="fas fa-flag"></i> Help</a></li>
				  </ul>
				</div>
			</div>
		</div>
	</div>
</div>

