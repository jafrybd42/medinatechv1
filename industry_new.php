<style>
  /*@media (max-width: 1260px)
{*/


  /* Style the tab */
  .tab {
    font-family: Poppins;
    font-weight: 400;
    overflow: hidden;
    border: 1px solid #f7f7f7;
    background-color: #f7f7f7;
    /*padding-bottom: 40px;*/
  }

  /* Style the buttons inside the tab */
  .tab button {
    background-color: inherit;
    float: center;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;
    font-size: 12px;
    font-family: Poppins;
    font-weight: 400;
  }

  /* Change background color of buttons on hover */
  .tab button:hover {}

  /* Create an active/current tablink class */
  .tab button.active {
    background-color: #f7f7f7;
    font-weight: bold;
    border-bottom: 3px solid black;
  }

  /* Style the tab content */
  .tabcontent {
    padding-top: 30px;
    padding-bottom: 30px;
    display: none;
    /*padding: 6px 12px;*/
    border: 1px solid #f7f7f7;
    border-top: none;
    padding-right: 20px;
  }

  .circular--landscape {
    display: inline-block;
    position: relative;
    width: 200px;
    height: 200px;
    overflow: hidden;
    border-radius: 50%;
    background: #fff;
    opacity: 1;
  }
</style>
</head>

<body>


  <!--
  <div class="tab" align="center">
    <button class="tablinks" onclick="openCity(event, 'London')" onclick="Bold()" id="defaultOpen" align="center"><img src="../images/book.png" height="50px" /><br> <br>Education</button>
    <button class="tablinks" onclick="openCity(event, 'Paris')" onclick="Bold()" id="texto" align="center"><img id="imgg" src="../images/Infrastructure.png" height="50px" /><br> <br>Infrastructure</button>
    <button class="tablinks" onclick="openCity(event, 'Tokyo')" onclick="Bold()" align="center"><img src="../images/Finance.png" height="50px" /><br> <br>Finance</button>
  </div>

  <div id="London" class="tabcontent">
    <img src="../images/industry_ed.png" align="center" class="industry_img" style="cursor: not-allowed;">
  </div>

  <div id="Paris" class="tabcontent">
    <img src="images/industry_infr.png" align="center" class="industry_img" style="cursor: not-allowed;">
  </div>

  <div id="Tokyo" class="tabcontent">
    <img src="images/industry_fin.png" align="center" class="industry_finance" style="cursor: not-allowed;">
  </div>

  <script>
    function openCity(evt, cityName) {
      var i, tabcontent, tablinks;
      tabcontent = document.getElementsByClassName("tabcontent");
      for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
      }
      tablinks = document.getElementsByClassName("tablinks");
      for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
        imgg.updateSrc = "images/book.png";
      }
      document.getElementById(cityName).style.display = "block";
      evt.currentTarget.className += " active";

    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();
  </script> -->
  <div class="form_">
    <small class="dropline" style="font-size:15px;">To know more about our projects or to request a demo <br>feel free to contact us</small>

    <div>
      <form action="../reach_us_config.php" method="post">

        <input class="formHalfBox" type="text" id="name" name="name" placeholder="Full Name">

        <input class="formHalfBox" type="text" id="email" name="email" placeholder="E-mail"> <br>

        <input class="formFullBox" type="text" id="subject" name="subject" placeholder="Subject">

        <textarea class="formFullBox txtarea" id="message" rows="4" cols="50" name="message" placeholder="Your Message ..."></textarea><br>

        <input type="submit" value="Send Message">

      </form>
    </div>

  </div>