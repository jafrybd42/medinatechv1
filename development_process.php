<link rel="stylesheet" type="text/css" href="css/development.css">
<script type='text/javascript' src='js/jquery.js'></script>
<script type='text/javascript' src='js/jquery2.js'></script>
<script type='text/javascript' src='js/jquery3.js'></script>
<!-- <script type="text/javascript" src="vendor/jquery/jquery.min.js" /> -->
<script type='text/javascript' src='js/jquery4.js'></script>
<?php
include('includes/header.php'); 
include('includes/navbar.php'); 
include('includes/scripts.php');
?>  
<!-- <div id="primary" class="content-area" style="
      ">
    <main id="main" class="site-main" role="main">

        
 -->
<!-- <section id="single-page" class="section our-process" style=""> -->
   <!--  <div class="container"> -->
            <div class="row single-page-content">
                                    <div data-vc-full-width="true" data-vc-full-width-init="true" class="vc_row wpb_row vc_row-fluid vc_custom_1516969804409 vc_row-has-fill" style="position: relative; box-sizing: border-box; width: 1349px; padding-left: auto; padding-right: auto;"><div class="wpb_column vc_column_container vc_col-sm-12" id="3333"><div class="vc_column-inner"><div class="wpb_wrapper"><div class="vc_row wpb_row vc_inner vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-3"><div class="vc_column-inner"><div class="wpb_wrapper"></div></div></div><div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner"><div class="wpb_wrapper">
    <div class="wpb_text_column wpb_content_element ">
        <div class="wpb_wrapper">
            <h6 style="text-align: center;">A quick overview of Softwared Solutions process and details of our workflow.</h6>

        </div>
    </div>
<div class="vc_empty_space" style="height: 25px"><span class="vc_empty_space_inner"></span></div><header class="kd-section-title col-lg-12 text-center    "><h1 class="separator_off">Our process</h1></header></div></div></div><div class="wpb_column vc_column_container vc_col-sm-3"><div class="vc_column-inner"><div class="wpb_wrapper"></div></div></div></div><div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1513705144894"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper">
            <div class="kd-process-steps process_four_elem process-checkbox-template ">
                <ul>
                <li class="pss-item">
                    <div class="pss-container"><div class="pss-step-number"><span>1</span></div><div class="pss-text-area">
                            <h4>Understanding a new product</h4>
                            <p>A detailed concept &amp; scope definition and evaluation of the project. We communicate with a client to identify the goals and business opportunities.</p></div>
                    </div>
                </li>
                <li class="pss-item">
                    <div class="pss-container  "><div class="pss-step-number"><span>2</span></div><div class="pss-text-area">
                            <h4>Appraisal for an existing product</h4>
                            <p>Our process will start from the audit of an existing solution. We evaluate the solution and identify possible improvements for UX design, functionality, security and business needs.</p></div>
                    </div>
                </li>
                <li class="pss-item">
                    <div class="pss-container  "><div class="pss-step-number"><span>3</span></div><div class="pss-text-area">
                            <h4>Engineering and product design</h4>
                            <p>Before actual development, we do the business analysis requirements elicitation and UX design. We hear our clients and create prototypes regarding business goals and defined specifications.</p></div>
                    </div>
                </li>
                <li class="pss-item">
                    <div class="pss-container  "><div class="pss-step-number"><span>4</span></div><div class="pss-text-area">
                            <h4>Product development and QA</h4>
                            <p>We start client &amp; server-side development using UX research and business analysis. In addition to that, we continue working on the UI design, delivering the final visual representation of the product. We guarantee excellent quality of code by continuous QA sessions.</p></div>
                    </div>
                </li></ul>
            </div></div></div></div></div></div></div></div></div></div><div class="vc_row-full-width vc_clearfix"></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper"><div class="feature-sections-wrapper ">
            <section class="feature-sections-tabs">
              <nav class="kd-feature-tabs">
                <ul class="nav nav-tabs sticky-tabs">
                <li class="nav-label"><a class="feature-tabs-scroll" href="#kd-fss-5e55d890db819">Research</a></li><li class="nav-label"><a class="feature-tabs-scroll" href="#kd-fss-5e55d890dc02c">Business Analysis</a></li><li class="nav-label"><a class="feature-tabs-scroll" href="#kd-fss-5e55d890dc667">Product design</a></li><li class="nav-label"><a class="feature-tabs-scroll" href="#kd-fss-5e55d890dcc41">Development</a></li><li class="nav-label"><a class="feature-tabs-scroll" href="#kd-fss-5e55d890dd1e2">QA</a></li></ul>
              </nav>
            </section>
            <section id="kd-fss-5e55d890db819" class="featured-right ">
              
              <div class="container">
                <div class="side-content-wrapper"><img src="images/development/new_product.png" height="45"> <h3 class="side-content-title">Understanding a new product</h3><div class="side-content-text"><p></p>
<ul>
<li>Concept definition and evaluation</li>
<li>The outlined vision of the project</li>
<li>Lean Canvas</li>
<li>Scope definition</li>
<li>Detailed product feature list based on Value Chain Map with modules, flows, and use cases.</li>
</ul>
<p></p></div></div><div class="side-featured-wrapper">
      <div class="featured-image"><img src="images/development/9.svg" class="attachment-full" alt=""></div>
    </div></div>
            </section><section id="kd-fss-5e55d890dc02c" class="featured-left ">
              
              <div class="container">
                <div class="side-content-wrapper"><img src="images/development/existing.png" height="45"> <h3 class="side-content-title">Appraisal for an existing product</h3><div class="side-content-text"><p></p>
<ul>
<li><strong>Code audit: </strong>A report with prioritized issues and a recovery plan with suggestions</li>
<li><strong>UX review: </strong>List of prioritized UX design issues with illustrative examples of best practices</li>
<li><strong>Exploratory testing: </strong>In-depth understanding of all features and integrations in the project and identification of all major discrepancies.</li>
</ul>
<p></p></div></div><div class="side-featured-wrapper">
      <div class="featured-image"><img src="images/development/10.svg" class="attachment-full" alt=""></div>
    </div></div>
            </section><section id="kd-fss-5e55d890dc667" class="featured-right ">
              
              <div class="container">
                <div class="side-content-wrapper"><img src="images/development/analysis.png" height="45"><h3 class="side-content-title">Business analysis and design</h3><div class="side-content-text"><p></p>
<ul>
<li><strong>Business analysis: [</strong>Business, Operational and Data models]</li>
<li>User flows, Wireframes and UX prototypes, Texts for user interfaces.</li>
<li>Interactive prototypes and mockups for all resolutions</li>
<li>UI assets: icons, banners, patterns, illustrations, animations</li>
<li>Design specification: style guide, UI kit</li>
</ul>
<p></p></div></div><div class="side-featured-wrapper">
      <div class="featured-image"><img src="images/development/11.svg" class="attachment-full" alt=""></div>
    </div></div>
            </section><section id="kd-fss-5e55d890dcc41" class="featured-left ">
              
              <div class="container">
                <div class="side-content-wrapper"><img src="images/development/development.png" height="45"> <h3 class="side-content-title">Product development</h3><div class="side-content-text"><p></p>
<p class="workflow-static-slide__title"><strong>Server/Client-side application development</strong></p>
<ul class="workflow-static-slide__list circle-list-gradient">
<li>Server-side programming with documentation</li>
<li>Database architecture design</li>
<li>Stable and secure architecture development</li>
<li>Client-side web or mobile application connected to server-side API</li>
</ul>
<p></p></div></div><div class="side-featured-wrapper">
      <div class="featured-image"><img src="images/development/12.svg" class="attachment-full" alt=""></div>
    </div></div>
            </section><section id="kd-fss-5e55d890dd1e2" class="featured-right ">
              
              <div class="container">
                <div class="side-content-wrapper"><img src="images/development/devops.png" height="45"> <h3 class="side-content-title">DevOps and QA</h3><div class="side-content-text"><p></p>
<p class="workflow-static-slide__title"><strong>Infrastructure management and DevOps</strong></p>
<ul class="workflow-static-slide__list circle-list-gradient">
<li>Infrastructure as code with Docker containers</li>
<li>Automation deployment script &amp; continuous integration setup</li>
<li>Production control and monitoring tools setup</li>
</ul>
<p>&nbsp;</p>
<p class="workflow-static-slide__title"><strong>QA</strong></p>
<ul class="workflow-static-slide__list circle-list-gradient">
<li>Fully tested working product</li>
<li>Test specification, test suites, test cases and automation testing scripts</li>
</ul>
<p></p></div></div><div class="side-featured-wrapper">
      <div class="featured-image"><img src="images/development/13.svg" class="attachment-full" alt=""></div>
    </div></div>
            </section>
            </div></div></div></div></div><div data-vc-full-width="true" data-vc-full-width-init="true" class="vc_row wpb_row vc_row-fluid" style="position: relative; left: auto; box-sizing: border-box; width: 1349px; padding-left: auto; padding-right: auto;"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper"><div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1516645814693"><div class="wpb_column vc_column_container vc_col-sm-3"><div class="vc_column-inner"><div class="wpb_wrapper"></div></div></div><div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner"><div class="wpb_wrapper">
    <div class="vc_empty_space" style="height: 15px"><span class="vc_empty_space_inner"></span></div><h3 style="text-align: center" class="vc_custom_heading vc_custom_1577694979176">Have any query ?</h3><a href="contact_us.php" target="_self" title="Contact" class="tt_button    button-center  "><span class="prim_text">Contact Us</span><span class=" iconita"></span></a></div></div></div><div class="wpb_column vc_column_container vc_col-sm-3"><div class="vc_column-inner"><div class="wpb_wrapper"></div></div></div></div></div></div></div></div></div>
   
                                               
<!--     </div>
</section> -->

<!--     </main> -->
    <!-- #main -->
<!-- </div> -->

<!-- #primary -->


  <?php

include('includes/footer.php');
?>

