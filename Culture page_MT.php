

<DOCTYPE html>
<html>
   <head>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>MedinaTech</title>
      <link href="https://fonts.googleapis.com/css?family=Poppins:600,700|Roboto&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="style_MT.css">
      <script src="functions_MT.js"></script>
      <!-- <link rel="stylesheet" type="text/css" href="css/videocontrols.css" /> -->
   </head>
   <body>
      <!-- --------------------tab menu------------------------- -->
      <div class="tabMenu">
         <div>
            <a href="index.php"><img class="tabMenu_logo" src="skins/MedinaTech PNG.png"></a>
         </div>
         <ul>
            <li><a  href="index.php"><b>Home Page</b></a></li>
            <li><a class="active" href="#"><b>Culture & Career Development</b></a></li>
            <li><a href="ReachUs_MT.php"><b>Reach Us</b></a></li>
         </ul>
      </div>
      <!-- ---------------mobile menu---------------------------------- -->
      <div class="mobileMenu">
         <a href="index.php"><img style="width: 140px; position: absolute;padding: 2px 10px;  z-index: -2" src="skins/MedinaTech PNG.png"></a>
         <div class="dropdown">
            <img onclick="myFunction()" class="dropbtn" style="width: 25px" src="skins/menu.png">
            <div id="myDropdown" class="dropdown-content">
               <a class="" href="index.php">Home</a>
               <a class="active" href="#">Culture & Career Development</a>
               <a href="ReachUs_MT.php">Reach Us</a>
            </div>
         </div>
      </div>
      <!-- ---------------pc navbar---------------------------------- -->
      <div class="navbar">
         <ul>
            <li><a href="ReachUs_MT.php"><b>Reach Us</b></a></li>
            <li><a class="active" href="#"><b>Culture & Career Development</b></a></li>
            <li><a class="" href="index.php"><b>Home Page</b></a></li>
         </ul>
         <a href="index.php"><img class="lionLogo" src="skins/lion.png"></a>
      </div>
      <!-- ------------------------------------ -->
      <div class="social">
         <a href="http://www.facebook.com/sharer/sharer.php?u=https://medinatech.co" target="_blank"><img src="skins/fb.png"></a>
         <a href="https://www.linkedin.com/shareArticle?mini=true&url=https%3A%2F%2Fwww.medinatech.co&title=Medina+Tech" target="_blank"><img src="skins/in.png">  </a>
      </div>
      <!-- --------------------------------------- -->
      <section>
         <!-- <div class="gridFull">
            <div class="allItem"> -->
         <!-- <div class="cultureHeader"> -->
         <img style="width: 100%; right: 0px; z-index: -1" src="skins/Group 15.png">
         <p class="heading" style="margin-top: -11%" >Culture & Career Development</p>
         <!-- </div> -->
         <!-- 
            </div>
            </div> -->
      </section>
      <br><br><br><br>
      <div class="container" align="center">
        <!--  <div class="grid2"> -->
            <section>
               <font align="center" size="3px">
               A startup culture is a workplace environment that values Creative problem solving, 
               <br/>
             </font>
             <font size="5px">
               <b>
               Open communication and A flat hierarchy.
               </b>
               </font>
               <img style="width: 80%; right: 0px; z-index: -1" src="skins/a-01@2x.png" >
            </section>
     <!--     </div> -->
      </div>
<!-- </div> -->
<br>
 <div class="container2" align="center">
    <font align="center" size="3px">
               We prioritize comfort & convenience of every team member through 
               <br/>
             </font>
             <font size="5px">
               <b>
               Efficient Work Environment | Empowerment & Career Development
               </b>
               </font>
               <br>
               <br>
<div class="rowt">
  <div class="columnt" align="right">
    <img src="images/efficient.png" alt="Snow" style="width:50%" align="right">
  </div>
  <div class="columnt" align="left">
   <a href="#apply">
    <img src="images/empowerment.png" alt="Forest" style="width:50%" align="left">
 </a>
  </div>
</div>





    </div>

      <?php
         include "footer_new.php";
         ?>
   </body>
</html>

