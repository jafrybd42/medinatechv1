<DOCTYPE html>
<html>
<head>
    <!--Redirect to different Pages Based On Different Screen Size-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
$(document).ready(function(){
  if($(window).width() <= 750) {
window.location = "http://www.medinatech.co/mobile/ReachUs_MT.php";
}
  if($(window).width() >= 1000 ) {
window.location = "http://www.medinatech.co/ReachUs_MT.php";
}
});
</script>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Medina Tech || Reach Us</title>
	
<!--Useful Links for font, Favicon-->
<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
        <link rel="apple-touch-icon" sizes="57x57" href="../images/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="../images/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="../images/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="../images/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="../images/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="../images/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="../images/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="../images/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="../images/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192" href="../images/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="../images/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="../images/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="../images/favicon-16x16.png">
        <link rel="manifest" href="../manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Medina Tech || Reach Us</title>
        <meta charset="utf-8" title="MedinaTech">

        <link href="https://fonts.googleapis.com/css?family=Poppins:600,700|Roboto&display=swap" rel="stylesheet">
        <!--CUSTOM CSS-->
        <link rel="stylesheet" href="../style_MT.css">
        <link rel="stylesheet" href="../Updated.css">
        <!--SCRIPTS-->
        <script src="../functions_MT.js"></script>

  <div class="tabMenu">

    <div>
      <a href="index.php"><img class="tabMenu_logo" src="../skins/MedinaTech PNG.png" alt="logo"></a>
    </div>

    <ul style="width: 95%;/*! margin-left: 10%; */">

        <li style="margin-left: 215px;"><a href="index.php"><b>Home Page</b></a></li>
        <li><a href="teams.php"  ><b>Culture &amp; Career Development</b></a></li>
        <li><a href="ReachUs_MT.php" class="active"><b>Reach Us</b></a></li>

      </ul>

  </div>

<!-- ---------------mobile menu---------------------------------- -->

  <div class="mobileMenu">

    <a href="index.php"><img style="width: 140px; position: absolute;padding: 2px 10px;" src="../skins/MedinaTech PNG.png" alt="logo"></a>

    <div class="dropdown">
      <img onclick="myFunction()" class="dropbtn" style="width: 25px" src="../skins/menu.png" alt="menu">
      
      <div id="myDropdown" class="dropdown-content">
        <a  href="index.php">Home</a>
        <a href="teams.php">Culture & Career Development</a>
        <a class="active" href="ReachUs_MT.php">Reach Us</a>
      </div>
    </div>
  </div>

  <!-- ---------------pc navbar---------------------------------- -->

  <div class="navbar">
      
      <ul>

        <li><a class="active" href="ReachUs_MT.php"><b>Reach Us</b></a></li>
        <li><a href="teams.php"><b>Culture & Career Development</b></a></li>
        <li><a href="index.php"><b>Home Page</b></a></li>

      </ul>
      <a href="index.php"><img class="lionLogo" src="../skins/lion.png" alt="logo"></a>
    
  </div>
<!--Reach Us Section -->
<div class="gridFull">
<div class="allItem">

  <div class="gridReachUs" style="height: 750px;">

  <div class="form_" style="width: 100px;height: 100%;margin-top: 80px;margin-left: -25px;">
      <p class="dropline" style="width: 250px;">DROP A LINE</p>

      <div>
        <form action="/action_page.php" style="height: ;">

          <input class="formHalfBox" type="text" id="fname" name="firstname" placeholder="Full Name">

          <input class="formHalfBox" type="text" id="lname" name="lastname" placeholder="E-mail"> <br>

          <input class="formFullBox" type="text" id="lname" name="lastname" placeholder="Subject">

          <input class="formFullBox" type="text" id="lname" name="lastname" placeholder="Message"><br>
        
          <input type="submit" value="Send Message">
        </form>
      </div>
    </div>
    </div>
  </div>

</div> <!-- allItem -->
</div> <!-- gridFull -->

<!--Adding Footer Section-->
 <?php
 include "../footer_new.php";
 ?>


</body>
</html>

