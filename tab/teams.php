<DOCTYPE html>
    <html>

    <head>
        <!--Refirect to different pages based on Screen Width-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
$(document).ready(function(){
  if($(window).width() <= 750) {
window.location = "http://www.medinatech.co/mobile/teams.php";
}
  if($(window).width() >= 1000 ) {
window.location = "http://www.medinatech.co/teams.php";
}
});
</script>
      <!-- Adding Stylesheet and Favicon -->

        <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
        <link rel="apple-touch-icon" sizes="57x57" href="../images/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="../images/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="../images/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="../images/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="../images/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="../images/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="../images/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="../images/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="../images/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192" href="../images/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="../images/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="../images/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="../images/favicon-16x16.png">
        <link rel="manifest" href="../manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Medina Tech || Career Development</title>
        <meta charset="utf-8" title="MedinaTech">

        <link href="https://fonts.googleapis.com/css?family=Poppins:600,700|Roboto&display=swap" rel="stylesheet">
        <!--CSS-->
        <link rel="stylesheet" href="../style_MT.css">
        <link rel="stylesheet" href="../Updated.css">
        <!--SCRIPTS-->
        <script src="../functions_MT.js"></script>
    </head>

<body>
 <!-- --------------------tab menu------------------------- -->
      <div class="tabMenu">
         <div>
            <a href="index.php"><img class="tabMenu_logo" src="../skins/MedinaTech PNG.png" alt="logo"></a>
         </div>
        <ul style="width: 95%;/*! margin-left: 10%; */">

        <li style="margin-left: 215px;"><a href="index.php"><b>Home Page</b></a></li>
        <li><a href="teams.php"  class="active"><b>Culture &amp; Career Development</b></a></li>
        <li><a href="ReachUs_MT.php"><b>Reach Us</b></a></li>

      </ul>
      </div>
      <!-- ---------------mobile menu---------------------------------- -->
      <div class="mobileMenu">
         <a href="index.php"><img style="width: 140px; position: absolute;padding: 2px 10px;  z-index: 1" src="../skins/MedinaTech PNG.png" alt="logo"></a>
         <div class="dropdown">
            <img onclick="myFunction()" class="dropbtn" style="width: 25px" src="../skins/menu.png" alt="menu">
            <div id="myDropdown" class="dropdown-content">
               <a class="" href="index.php">Home</a>
               <a class="active" href="#">Culture & Career Development</a>
               <a href="ReachUs_MT.php">Reach Us</a>
            </div>
         </div>
      </div>
      <!-- ---------------pc navbar---------------------------------- -->
      <div class="navbar">
         <ul>
            <li><a href="ReachUs_MT.php"><b>Reach Us</b></a></li>
            <li><a class="active" href="#"><b>Culture & Career Development</b></a></li>
            <li><a class="" href="index.php"><b>Home Page</b></a></li>
         </ul>
         <a href="index.php"><img class="lionLogo" src="../skins/lion.png"></a>
      </div>
      <!-- ------------------------------------ -->
      <div class="social">
         <a href="http://www.facebook.com/sharer/sharer.php?u=https://medinatech.co" target="_blank"><img src="../skins/fb.png" alt="logo"></a>
         <a href="https://www.linkedin.com/shareArticle?mini=true&url=https%3A%2F%2Fwww.medinatech.co&title=Medina+Tech" target="_blank"><img src="../skins/in.png" alt="logo">  </a>
      </div>


      <section>
       
            <img style="width: 100%; right: 0px; z-index: -1;padding-top: 70px;
height: 160px;" src="../mobile/images/bg.png" alt="background">
            <p class="heading" style=" font-size: 30px; margin-top: -12%;

font-size: 30px;

margin-left: 15px;">Culture &<br> Career Development</p>
       
        </section>
        <br>
        <br>
        <br>
        <br>
        <div class="container" align="center">
           
            <section>
                <font align="center" size="3px" style="width: 80%;
font-size: 15px;
display: block;
text-align: center;
font-family:  Roboto;
letter-spacing: 0;
color: #000000;
opacity: 1;">A startup culture is a workplace environment that values <br><b> Creative problem solving, Open communication and A flat hierarchy.</b>
               </font>
               
                <br>
                <img style="cursor: not-allowed;width: 90%; right: 0px; z-index: -1" src="../skins/a-01@2x.png" alt="logo">
            </section>
            
        </div>
    </div>
        
        <br>
        <br>
            <br>
         <div class="container" align="center" style="margin-top: -70px; background: #f7f7f7;max-width: 100%;">
              <section style="padding-bottom: 10px;">
                <font align="center" size="3px" style="padding-top: 50px;
                width: 80%;
font-size: 15px;
display: block;
text-align: center;
font-family:  Roboto;
letter-spacing: 0;
color: #000000;
opacity: 1;
padding-bottom: 20px;
">We prioritize comfort & convenience of every team member through<br><b>Efficient Work Environment | Empowerment & Career Development</b>
               </font>
                
                <br>
                <img src="../images/efficient.png" alt="Snow" style="cursor: not-allowed;width:55%; padding-bottom: 20px;" align="center" > <br>
                  <a href="#popup1">
                <img src="../images/empowerment.png" alt="Forest" style="cursor: pointer;width:55%; padding-bottom: 20px;" align="center">
                </a>
            </section>
          
        </div>

        <div class="container" align="center" style="background: white; ">
        <!---->
           <!--Team section-->
<!---->
        </div>
        <!--Adding Footer Section-->
        <?php
          include "../footer_new.php";
        ?>
        
         <style>
/*body {*/
/*  font-family: Arial, sans-serif;*/
/*  background: url(http://www.shukatsu-note.com/wp-content/uploads/2014/12/computer-564136_1280.jpg) no-repeat;*/
/*  background-size: cover;*/
/*  height: 100vh;*/
/*}*/

h1 {
  text-align: center;
  font-family: Tahoma, Arial, sans-serif;
  color: #06D85F;
  margin: 80px 0;
}

.box {
  width: 40%;
  margin: 0 auto;
  background: rgba(255,255,255,0.2);
  padding: 35px;
  border: 2px solid #fff;
  border-radius: 20px/50px;
  background-clip: padding-box;
  text-align: center;
}

.button {
  font-size: 1em;
  padding: 10px;
  color: #fff;
  border: 2px solid #06D85F;
  border-radius: 20px/50px;
  text-decoration: none;
  cursor: pointer;
  transition: all 0.3s ease-out;
}
.button:hover {
  background: #06D85F;
}

.overlay {
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background: rgba(0, 0, 0, 0.7);
  transition: opacity 500ms;
  visibility: hidden;
  opacity: 0;
}
.overlay:target {
  visibility: visible;
  opacity: 1;
}

.popup {
  margin: 70px auto;
  padding: 20px;
  background: #fff;
  border-radius: 5px;
  width: 30%;
  position: relative;
  transition: all 5s ease-in-out;
}

.popup h2 {
  margin-top: 0;
  color: #333;
  font-family: Tahoma, Arial, sans-serif;
}
.popup .close {
  position: absolute;
  top: 20px;
  right: 30px;
  transition: all 200ms;
  font-size: 30px;
  font-weight: bold;
  text-decoration: none;
  color: #333;
}
.popup .close:hover {
  color: #06D85F;
}
.popup .content {
  max-height: 30%;
  overflow: auto;
}

@media screen and (max-width: 700px){
  .box{
    width: 70%;
  }
  .popup{
    width: 70%;
  }
}
               </style>
<div id="popup1" class="overlay">
	<div class="popup">
		<h2>Alert !</h2>
		<a class="close" href="#">&times;</a>
		<div class="content">
		    <br>
		    
		    <br>
			We are currently not accepting any internships due to changes in our project scope for the pandemic.
		</div>
	</div>
</div>
</body>
</html>