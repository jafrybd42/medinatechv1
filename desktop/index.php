<html>

<head>
  <!--PWA-->
  <!-- <link rel="manifest" href="../manifest.json">
    <script src="../index.js" type="module"></script> -->



  <!--Redirect to defferent pages based on screen size-->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
  <script>
    $(window).on('load resize', function() {
      // $(document).ready(function(){
      if ($(window).width() <= 750) {
        window.location = "../mobile/index.php";
      }
      if ($(window).width() > 750 && $(window).width() < 1000) {
        window.location = "../tab/index.php";
      }
    });
  </script>


  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-8TFWYJPDBS"></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());
    gtag('config', 'G-8TFWYJPDBS');
  </script>



  <!--Useful font, Icon, Tag -->
  <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
  <link rel="apple-touch-icon" sizes="57x57" href="../images/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="../images/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="../images/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="../images/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="../images/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="../images/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="../images/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="../images/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="../images/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192" href="../images/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="../images/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="../images/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="../images/favicon-16x16.png">
  <!-- <link rel="manifest" href="../manifest.json"> -->
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="../ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Medina Tech</title>
  <meta charset="utf-8" title="MedinaTech">

  <link href="https://fonts.googleapis.com/css?family=Poppins:600,700|Roboto&display=swap" rel="stylesheet">

  <!--CSS-->
  <link rel="stylesheet" href="../style_MT.css">
  <link rel="stylesheet" href="../Updated.css">
  <!--SCRIPTS-->
  <script src="../functions_MT.js"></script>

</head>

<body>

  <!--Nav View For different types of screen size-->
  <?php
  include "../nav_view.php";
  ?>
  <!--Top Image, Title and Description-->
  <div class="gridFull">
    <div class="allItem">
      <section class="homepageHeaderSec">
        <div style="" class="grid1">

          <div class="item1">
            <img class="graph1" style="cursor: not-allowed;width: 100%;padding-top: 70px;padding-bottom:70px;" src="../skins/FULL2.gif" alt="top image">
            <img class="graph2" style="cursor: not-allowed;margin-right: auto; margin-left: 30px;margin-right: auto;" src="../skins/FULL2(mobile).gif" alt="top image">
          </div>

          <div class="item2">
            <p class="bigText" style="    font-family: Poppins;
    font-size: 40px;
    letter-spacing: 0;
    color: #000000;
    text-shadow: 3px 3px 6px #00000029;
    opacity: 1;
    font-weight: 600;
    ">
              We innovate <br>
              We work together <br>
              We solve problems <br>
            </p>

            <p style="font-family: 'Roboto', sans-serif; font-size: 15px; ">
              <br><br>
              Medina Tech is a Software Company, founded in February 2020, operating out of Dhaka Bangladesh. Medina Tech’s main focus is work on projects that uphold Technology for Social Good, therefore Medina Tech specializes in building Education Technology Solutions.
<br>Our  values entail to prioritize UI/UX, Accessibility, Security & proactive Customer Support for all software solutions.
              <br><br>
            </p>
            <!--Useful Buttons-->
            <a href="#" class="button" style="background-color: black; color: white; border-color: black; width: 150px; font-family: Poppins;text-align: center;
font: SemiBold 20px/30px Poppins;
letter-spacing: 0;
color: white;
opacity: 1;background: #000000 0% 0% no-repeat padding-box;
box-shadow: 3px 3px 6px #00000029;
opacity: 1;
cursor: not-allowed;"><b>Work with Us !</b></a>

            <a href="#" class="button" style="background: transparent linear-gradient(117deg, #D7B722 0%, #D7B722 100%) 0% 0% no-repeat padding-box;
box-shadow: 3px 3px 6px #00000029;
opacity: 1; text-align: center;
font-family:  'Poppins';
border-color: #D7B722;
letter-spacing: 0;
color: #000000;
opacity: 1;
cursor: not-allowed;"><b>Innovate with Us !</b></a>
          </div>
        </div>
      </section>
      <!-- ----------How we work------------------------ -->
      <section>
        <img style="cursor: not-allowed;width: 100%; right: 0px; z-index: -1" src="../skins/Group 15.png" alt="background">
        <p class="heading_new" style="
font: SemiBold 80px/120px Poppins;
letter-spacing: 0;
color: #000000;
text-shadow: 5px 5px 6px #00000029;
opacity: 1;
font-size:60px;">How We Work ?</p>

        <div class="container">
          <p style="
font-family:  Poppins;
letter-spacing: 0;
color: #000000;
opacity: 1;
font-size: 34px;
padding-top: 30px;
font-weight:550" align="center">
            <font color="black">Have an Idea ? </font> - <font color="#D7B722">Have a Problem ?</font>
          </p>
          <div class="item4">
            <img src="../skins/Group 5.png" alt="Cover" style="cursor: not-allowed;margin-top: 0px;

width: 100%;

margin-left: -20px;

padding-bottom: 30px;

height: 320px;
width: 420px;
margin-left: 350px;
margin-top:30px;">
          </div>
          <div class="grid2" style="margin-bottom: 70px;margin-left: 40px;">
            <div class="item3" style="margin-top: -227px;
margin-left: -150px;grid-area: rightp-end;">
              <p style="background: #000000 0% 0% no-repeat padding-box;
opacity: 1;
text-align: center;
font-family: Poppins;
font-weight: 600;
letter-spacing: 0;
color: #FFFFFF;
opacity: 1;
width: 100px;
padding: 10px;

font: SemiBold Poppins;
letter-spacing: 0;
color: #FFFFFF;
opacity: 1;
margin-bottom: -17px;
position: absolute;
margin-top: 5px;
margin-left: 26px;
" align="right">Solutions</p>
              <br>
              <p style="

          text-align: left;
font-family: Roboto;
letter-spacing: 0;
color: #303030;
font-size: 14px;
font-weight: 400;
opacity: 1; background: #FFFFFF 0% 0% no-repeat padding-box;
box-shadow: 0px 3px 15px #00000029;
border-radius: 5px;
opacity: 1; padding: 50px;
          height: 100px;
          width: 300px;
          /*! margin-right: -660px; */
          margin-top: 10px;
">Our team of engineers
                are here to help our
                clients create software
                solutions. If you
                belong to an industry
                that is a part of our
                concern, we will help
                you find the most
                optimal- manageable
                and updated solutions
                using latest tech stacks</p>
            </div>

            <div class="item5" style="margin-left: -350px;grid-area: rightp-start;">
              <p style="background: #D7B722 0% 0% no-repeat padding-box;
opacity: 1;

text-align: center;
font-weight: 600;
font-family:Poppins;
letter-spacing: 0;
color: #FFFFFF;
opacity: 1;
margin-bottom: -17px;
width: 100px;
padding: 10px;
margin-left: 405px;
z-index: 2;
position: absolute;
margin-top: 5px;
">Innovations</p>
              <br>
              <p style="text-align: left;
font-family: Roboto;
font-weight: 400;
letter-spacing: 0;
color: #303030;
opacity: 1;
font-size: 14px;
background: #FFFFFF 0% 0% no-repeat padding-box;
box-shadow: 0px 3px 15px #00000029;
border-radius: 5px;
opacity: 1;
padding: 50px;
height: 100px;
width: 300px;
margin-left: 150px;
z-index: -2;
margin-top: 10px;
">Our team of engineers
                are always researching
                to innovate by building
                specialized in-house
                products for our
                industries of concern.
                We also welcome,
                people with ideas to
                come to us &amp;
                collaboratively innovate
                by using our resources.</p>
            </div>

          </div>

        </div>
      </section>
    </div>

    <!-- --------------------------------------------- -->


    <!-- ------------Industry Section------------------- -->
    <section style="background: #f7f7f7">
      <!--  <img style="width: 100%; right: 0px; z-index: -1" src="skins/Group 15.png"> -->
      <p class="heading" style="text-align: left;
    font-weight: 600;
font-family: Poppins;
letter-spacing: 0;
color: #000000;
text-shadow: 5px 5px 6px #00000029;
opacity: 1;
font-size:45px;"> Projects<br>-</p>

      <div class="container" style="width: 100%; background: #f7f7f7" align="center">
        <div class="grid2" style="grid-template-areas: 'center';width: 100%; margin-left: -40px" align="center">
          <!-- <div class="rowt" style="width: 100%"> -->

          <?php
          include "../industry_new.php";
          ?>
          <!-- </div> -->
        </div>

      </div>

    </section>


    <!-- ---Video End Part------ -->

    <section>
      <div class="lastPortion" style="margin-top: -1%">

        <div class="ltext">
          <p class="bigText" style="font-family: Poppins;
        font-weight: 600;
        line-height: 40px;
        letter-spacing: 0;
        opacity: 1;">“<span style="color:#D7B722">Code</span> speaks<br>
            Louder than words”</p>

          <p style="
      font-family: Roboto;
      font-weight: 400;
      font-size: 15px;
      letter-spacing: 0;
      color: #000000;
      opacity: 1;">
            <br>
            In Medina Tech, we aim to create a community along
            with our projects & solutions, to enhance the Goals of
            a Digital Bangladesh!
          </p>
        </div>

        <div class="lvideo" style="width: 700px;
    height: 350px;cursor: not-allowed;">

          <!-- iframe width="100%" height="100%" src="https://www.youtube.com/embed/Vw7KeY-YOno" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style=" background: #FFFFFF 0% 0% no-repeat padding-box;
      box-shadow: 0px 3px 20px #00000029;
    opacity: 1;"></iframe>onclick="this.paused ? this.play() : this.pause();"  -->

          <video autoplay="" loop="" muted="" playsinline="" src="../details.mp4" style="cursor: not-allowed" type="video/mp4" style="background: #FFFFFF 0% 0% no-repeat padding-box;
      box-shadow: 0px 3px 20px #00000029;
    opacity: 1;"></video>


        </div>


      </div>

    </section>




    <!-- --------------------------Footer--------------------------------- -->

    <?php
    include "../footer_new.php";
    include('../includes/scripts.php');
    ?>



    <style type="text/css">
      .splashSection--videoContainer {
        width: 40px;
        height: 50%;
        -webkit-box-flex: 2;
        -ms-flex: 2;
        flex: 2;
        -ms-flex-preferred-size: auto;
        flex-basis: auto;
        box-shadow: 0 4px 16px 0 rgba(31, 51, 51, 0.1), 0 2px 8px 0 rgba(31, 51, 51, 0.2)
      }
    </style>


  </div>
  </div>
</body>

</html>